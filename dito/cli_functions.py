# this script holds the main functions performed by dito
# main.py calls cli.py (the command line stuff) which 
# then calls these functions and passes them the args and config objects




import sys
import argparse
import os
import pandas as pd
import numpy as np

import glob

from . import const
from .cli_texts import d as clit

from functools import reduce, partial


import logging 
log = logging.getLogger('dito')


# we load some functions from other scripts
from . import rmlint_reports as rr
ls_report = rr.ls_report
generate_report = rr.generate_report
digest_report = rr.digest_report

from . import store as storelib


from .filescanner import Filescanner


# for when called in the cli tool
def update_assumptions(args, config_object):
    config_object.dbo.update_assumptions()



def ditoschool(args, config_object):
    installdir=config_object['DEFAULT'].get('installdir',None)
    schoolpathdir = os.path.join(installdir,"ditoschool")

    file = os.path.join(schoolpathdir, args.subject + ".md")
    with open(file,'r') as fi:
        for line in fi:
            print(line.rstrip())

def cleandbf(args, config_object):
    print("Execute the following to remove dito's database ! ARE YOU SURE ???")
    print(f"rm {config_object.dbpath}")

def hardlinkall(args, config_object):
    config_object.dbo.hardlink_all()


def filescannerf(args, config_object):
    """The function that scans the system for new files"""

    args.path = os.path.abspath(args.path)
    log.info(f"Looking for undiscovered files inside {args.path}")
    fs = Filescanner(config_object)

    paths_generator = fs.scanpath(f"{args.path}")
    e=0
    for e,path in enumerate(paths_generator):
        fs.digest_fp(path)
    log.info(f"Done. Found {e} paths in total.")

    log.info("Updating reasonable knowledge table.")
    config_object.dbo.update_rk( args, config_object )
    log.info(f"Scanning finished successfully. Don't forget to annotate your files.")









def foo_choose_table(args, config_object):

    if args.mode.startswith('mpf'):
        files = config_object.dbo.dptable_getinit("files")
        paths = config_object.dbo.dptable_getinit("paths")
        df = paths.merge(files, how='left', left_on="file_id",right_on="index",
            suffixes=("_paths","_files"))
        if args.mode.startswith('mpfi'):
            df = df.drop(["file_id","disk_id"],axis=1)

    elif args.mode.startswith('mpa'):
        paths = config_object.dbo.dptable_getinit("paths")
        assumed = config_object.dbo.dptable_getinit("assumed")
        df = paths.merge(assumed, how='left', left_on="path",right_on="path",
            suffixes=("_paths","_assumed"))
        if args.mode.startswith('mpai'):
            df = df.drop(["file_id"],axis=1)

    elif args.mode.startswith('mfm'):
        files = config_object.dbo.dptable_getinit("files")
        manual = config_object.dbo.dptable_getinit("manual")
        df = files.merge(manual, how='left', left_on="ghash",right_on="ghash",
            suffixes=("_files","_manual"))

    elif args.mode.startswith('mprk'):
        files = config_object.dbo.dptable_getinit("files")
        paths = config_object.dbo.dptable_getinit("paths")
        rk = config_object.dbo.dptable_getinit("rk")

        m1 = paths.merge(files[['ghash']], how='left', left_on="file_id",right_on="index",
            suffixes=("_paths","_files"))

        df = m1.merge(rk, how='left', left_on="ghash",right_on="ghash",
            suffixes=("_mfp","_rk"))
        if args.mode.startswith('mprki'):
            df = df.drop(["file_id","ghash"],axis=1)
            if args.mode.startswith('mprkii'):
                df = df.drop(["ignore","exists","done","quality","status"],axis=1)

    elif args.mode.startswith('rk'):
        df = config_object.dbo.dptable_getinit('rk')

        if args.mode.startswith('rkf'):
            print("This is a composite table, each unique checksum (ghash) can be associated with more paths.")
            rk = df.copy()
            files = config_object.dbo.dptable_getinit("files").loc[:,['ghash']]
            paths = config_object.dbo.dptable_getinit("paths").sort_values(by='path')         
            
            temp = files.merge(rk, how='left', left_on='ghash',right_on='ghash')
            temp2 = temp.merge(paths[['file_id','path']], how='left', left_index=True,right_on='file_id')

            df = temp2.drop_duplicates('ghash').drop(['extension','file_id'], axis=1)

            

        if 'rki' == args.mode:
            df = df.loc[:,["ghash","organism","tissue","stage","treatment","technique","target","replicate","tags","filenum"]]

    # if args.mode.startswith('mpf'):
    #     files = config_object.dbo.dptable_getinit("files")
    #     paths = config_object.dbo.dptable_getinit("paths")
    #     df = paths.merge(files, how='left', left_on="file_id",right_on="index")


    # elif args.mode.startswith('rkp'):
    #     files = config_object.dbo.dptable_getinit("files")
    #     rk = config_object.dbo.dptable_getinit("rk")

    #     m1 = pd.merge(left=files[['path','thash']],right=rk, how='left',on='thash')
    #     df = m1

    #     if args.mode.startswith('rkpi'):
    #         cols = ['path','organism', 'tissue', 'stage', 'treatment', 'technique', 'target', 'replicate', 'filenum','quality']
    #         existing = df['exists'].astype(bool)
    #         notskipped = ~df['skip'].astype(bool)
    #         df = df.loc[existing & notskipped,cols]

    elif args.mode in ["files","paths","manual","assumed"]:      
        df = config_object.dbo.dptable_getinit(args.mode)


    else:
        sys.exit("Table name not recognized. Available are: files,paths,manual,assumed,rk,rkp,rkpi")

    return df


def foo_filter_df(df,args, config_object):

    if 'path' in df.columns:
        prx = PathRegexer(args.regex,args.regexv)
        df = df[df.path.apply(prx.test)]
   
    if args.filter:
        df = filterdf(df, args.filter)
    if args.loc:
        df = locdf(df, args)

    if args.sa:
        if ',' in args.sa:
            args.sa = args.sa.split(',')
        df = df.sort_values(by=args.sa, ascending=True)
    elif args.sd:
        if ',' in args.sd:
            args.sd = args.sd.split(',')
        df = df.sort_values(by=args.sa, ascending=False)

    # if args.pcf:
    #     if 'path' in df.columns:
    #         df['path'] = df['path'].apply(lambda fp: "..."+"/".join(fp.split("/")[args.pcf+1:]))

    return df





def print_tables(args, config_object):
    """This is the background function for 'dito ls'.
    """
    # if args.examples:
    #     print(clit.tables.examples)
    #     return

    # choose the table
    df = foo_choose_table(args, config_object)
 
    # further filters
    df = foo_filter_df(df, args, config_object)

    if args.g:
        for gn,g in df.groupby(args.g):
            print(f"{gn}: {len(g)} rows")
        return 0

    if args.mountpoint:
        mount(args,config_object,df)
        return 0

    display_this_table(df, args)

    if args.to_csv:
        df.to_csv(args.to_csv)


    return 0



def backup_to_nas(args, config_object):
    import datetime
    import subprocess

    log.info("Backing up files to nas")

    original_path = os.getcwd()
    timestamp = datetime.datetime.now().strftime("%d-%m-%Y")
    
    ## makde filelist in temp file
    # this is the list of file that we're backing up
    file_list_name = f"backup_file_list.{timestamp}.txt"
    logspath = os.path.join(config_object['DEFAULT']['BASEDIR'], 'logs')
    file_list_path = f"{logspath}/{file_list_name}"

    # first, we put in everything in dito's basedir
    basedir = config_object['DEFAULT']['basedir']
    findcommand  = f"find {basedir} > {file_list_path}"
    log.info(f"Adding dito's files in {file_list_path}")
    process = subprocess.check_output(findcommand,shell=True)  

    # then, we'll search for fastq files with the same script we use
    # for dito's scan. That includes some nice thing like blacklisting some directories
    # that are full of crap
    # get in the folder containing the scripts
    findcoderoot = os.path.join(config_object['DEFAULT']['installdir'], "findfqgz")
    os.chdir(findcoderoot)
    log.info(f"Adding fastq.gzs in {file_list_path}")
    findcommand  = f"./findfqgz.sh /home/ska/panos >> {file_list_path}"
    process = subprocess.check_output(findcommand,shell=True)  

    # cd in dito backup scripts folder
    code_root = os.path.join(config_object['DEFAULT']['installdir'], "backup_scripts")
    os.chdir(code_root)

    log.info(f"Running backup scripts, logs can be found timestamped in {logspath}")
    ss = f"bash backup_to_nas.sh admin@192.168.49.95 /share/CACHEDEV1_DATA/Backups/nodos/ {logspath} {file_list_path}"
    process = subprocess.Popen(ss, stdout=subprocess.PIPE, shell=True)  

    # https://stackoverflow.com/questions/2804543/read-subprocess-stdout-line-by-line
    # this way we catch bash lines as they come and print them ourselves
    while True:
        line = process.stdout.readline().decode().rstrip()
        if not line:
            break
        #the real code does filtering here
        log.info(line)

    os.chdir(original_path)


def update_rk(args, config_object):
    # config_object.dbo.update_rk( args, config_object )

    config_object.dbo.dump_tables()
    
    
def safermhardlinks(args, config_object):
    log.debug("Will try to remove all files found in the hardlink backup folder")

    hlpath = os.path.join( config_object['DEFAULT']['basedir'], "hardlinks")
    
    lof = glob.glob(f"{hlpath}/*")

    for fp in lof:
        fi = os.open( fp, os.O_RDWR )
        info = os.fstat(fi)
        if info.st_nlink >1:
            log.debug(f"Removing {fp}")
            os.remove(fp)
        else:
            log.warning(f"NOT A HARDLINK {fp}, skipping")
            pass



def annotatef(args, config_object):
    """
    Manually add annotations to paths.
    We take all the existing paths, and maybe filter them with --do (downstream only)
    or regex expressions
    When we manually annotate, we use the path, but are annotating the underlying data (checksum)
    If I have two copies of a file, either normal or hardlink, if I annotate one of them
    I want the annotation to hold for the other as well.
    For this, the manual annotation is connected to the checksum of a file

    For each path, find the inode it currently points to, get the checksum,
    get the rk table values of the checksum, that's what we know from manual 
    and assumptions

    For the paths we have, add annotations manually, and potentially inherit
    from the rk table (which got values from assumptions on this path
    or even other paths that point to the same checksum)

    After we've manually annotated the path, we save the annotation on the manual
    table, which connects the annotation to the checksum.

    
    """

    log.debug(f"Searching for files to annotate")
    cwd = os.path.abspath(os.getcwd())
    # load the tables
    filesdf = config_object.dbo.dptable_getinit('files') 
    filesdf = filesdf[filesdf['exists'].astype(bool)]
    rkdf = config_object.dbo.dptable_getinit('rk') 
    manualdf = config_object.dbo.dptable_getinit('manual') 

    pathsdf = config_object.dbo.dptable_getinit('paths') 
    pathsdf = pathsdf[pathsdf['exists'].astype(bool)]
    # keep only the files downstream of here
    if args.path:
        pathsdf = pathsdf[pathsdf.path.str.startswith(os.path.abspath(args.path))]
        if len(pathsdf)<1:
            sys.exit("No dito files found to annotate downstream of here")

    # filter the pathsdf with the regular expressions
    prx = PathRegexer(args.regex,args.regexv)
    # these are the paths we might annotate
    pathsdf = pathsdf[pathsdf.path.apply(prx.test)]

    if args.inpath:
        for thing in args.inpath:
            pathsdf = pathsdf[pathsdf.path.apply(lambda path: thing in path)]

    if args.notinpath:
        for thing in args.notinpath:
            pathsdf = pathsdf[pathsdf.path.apply(lambda path: thing not in path)]
        
    # make some merge dfs
    # connect paths to inode/checksums
    m1 = pathsdf[['file_id','path']].merge(filesdf[['ghash']], how='left',left_on="file_id",right_on="index")
    # commect m1 to rk table through checksums
    m2 = m1.merge(rkdf, how='left',left_on="ghash",right_on="ghash").drop(['extension','file_id'],axis=1)
    
    tp = m2 #.drop(['ghash'],axis=1)
    
    COLUMNSm = ["organism","tissue","stage","treatment","technique","target","replicate","filenum","tags"]
    COLUMNS = ["path","done"] + COLUMNSm
    
    log.debug(f"Here are the files passing your filters:")
    display_this_table( tp[COLUMNS],
        args)

    log.debug(f"Looking for manually set values")
    updated_manual_rows=[]
    # for each row (path that has been selected)
    for irow,row in m2.iterrows():
        p = row.path
        h = row.ghash
        # for each path, i'll annotate its ghash on the manual table
        # get the manual row
        mslice = manualdf[manualdf.ghash==h].copy()
        assert len(mslice)==1, f"i don't know why but I found !=1 number of rows for checksum {h} in the manuals table"
        mrow = mslice.iloc[0].copy()

        # for each attribute
        for arg in COLUMNSm:

            if not args.noinherit:
                # if inherit, we take the value of the rk table into the manual table
                if arg in row:
                    mrow[arg] = row[arg]
            else:
                mrow[arg] = "NonE"

            # if i gave a value through the cli, override
            if getattr(args, arg) :
                mrow[arg] = getattr(args, arg)
            # for some reason, 'done' is not found with getattr
            if args.done:
                mrow['done'] = True
            else:
                mrow['done'] = False
        mrow['path'] = p

        updated_manual_rows.append(mrow)
    # cast the list of rows into a df
    upmandf = pd.DataFrame(updated_manual_rows).drop_duplicates("ghash")

    flag = False
    for arg in COLUMNSm:
        if getattr(args,arg):
            flag=True
    if flag:
        log.debug(f'Here are the affected manual rows')
        # upmandf = upmandf.drop("ghash",axis=1)
        
        # cs = list(upmandf.columns)
        # cs = [x for x in tp if x in cs]
        cs = ['ghash'] + COLUMNS

        display_this_table(upmandf[cs].drop_duplicates("ghash"), args)

        # display_this_table(manualdf.update(upmandf), args)
    # print(manualdf[manualdf.ghash.isin(upmandf.ghash.values)])

        if not args.commit:
            answer = input(f"Type 'commit' now to store the changes if you like them. Type anything else to exit  \n")
            if 'commit' == answer:
                args.commit = True

        if args.commit:
            # store the changes
            log.info("Storing changes to database")
            manualdf.update(upmandf)
            # config_object.dbo.dptable_safeput(config_object.dbpath, 'manual', manualdf)
            config_object.dbo.ptable( tablename='manual', df=manualdf, if_exists="replace")

            log.info("Updating reasonable knowledge table")
            # update the reasonable knowledge table
            # this updates the entire database because we changed a single row
            # it's extremely wasteful but at least it should work
            config_object.dbo.update_rk( args, config_object )

            

            
def display_this_table(table, args):
    df = table.copy()

    # display the thing
    pd.set_option('display.max_rows', args.mr)
    pd.set_option('display.min_rows', args.mr)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', args.mcw)


    if args.pcf:
        if 'path' in df.columns:
            df['path'] = df['path'].apply(lambda fp: "..."+"/".join(fp.split("/")[args.pcf+1:]))
            df.loc[df['path'].str.endswith("fastq.gz"),'path'] = df.loc[df['path'].str.endswith("fastq.gz"),'path'].apply(lambda x:x[:-9])
            df.loc[df['path'].str.endswith("fq.gz"),'path'] = df.loc[df['path'].str.endswith("fq.gz"),'path'].apply(lambda x:x[:-6])
        if 'ghash' in df.columns:
            df['ghash'] = df['ghash'].apply(lambda x: x[-6:])

    print(df.head(args.mr))

    if args.mr<len(df):
        print("Some rows were hidden. Set -mr to a bigger value to see them.")


    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', None)




    # print(m2.drop(['ghash'],axis=1))


    # print("\n\nHere are the changes, according to your arguments")
    # print(df)

    # if args.commit:
    #     # df['done'] = df['done'].astype(bool)
    #     config_object.dbo.safe_update_df_slice(config_object.dbpath, 'manual',df)
    #     print("\n\nChanges were successfully saved!")
    # else:
    #     print("\n\nIf you are happy with the changes, commit them to the database by passing the --commit option")














































# def mount_dicto(mp,stats, indent=''):
#     import pathlib
#     mp = os.path.abspath(mp)
#     mountroot = os.path.join(mp,"dito_virtual_fs")

#     for (d, s) in stats.items():
        
#         folder = f"{mountroot}{d}/"
#         if not os.path.isdir(folder):
#             pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
#         # for leaf in s.leafs:
#         #     print(f"{indent}{leaf}")

#         # for reportl in s.report:
#         #     print(f"{indent}{reportl}")

#         mount_dicto(mp,s, indent)
#         for leaf in s.leafs:
#             file = f"{mountroot}{leaf}"
#             original_path = "/home/ska/panos/dito/dito/empty.fastq.gz"
#             os.link(original_path, file)
  

# args fin "organism","technique","filenum","replicate"

# def mount_groups(mp,args ):
#     import pathlib
#     mp = os.path.abspath(mp)
#     mountroot = os.path.join(mp,"dito_virtual_fs")


#     FIELDS_PUT_IN_NAME = args.fin.split(",")
#     # FIELDS_GROUPBY = ["Organism","Tissue","Stage"]
#     FIELDS_GROUPBY = args.by.split(",")
    
#     df["filename"] = (df
#             .loc[:,FIELDS_PUT_IN_NAME+["filetype"]]
#             .apply(lambda x: "_".join(x[:-1])+f".{x[-1]}", axis=1))

#     reductionlist = [[(f"{MOUNTPOINT}",df)]] + FIELDS_GROUPBY
#     things = reduce(foo, reductionlist)

#     # just print
#     lot =[]
    
#     reductionlist = [[(f"{MOUNTPOINT}",df)]] + FIELDS_GROUPBY
#     things = reduce(foo, reductionlist)    
    
#     if not silent:
#         lof = []    
#         for folder,items in things:
#             for e,f in enumerate(folder.split("/")):
#                 try:
#                     if lof[e]==f:
#                         continue
#                     else:
#                         lof[e]=f
#                         print(buz(e),f)
#                 except IndexError:
#                     lof.append(f)
#                     print(buz(e),f)
#                     # print("EXCEPTION!!!")
#             e+=1
#             for irow,row in items.iterrows():
#                 print(buz(e),row.filename)

#     if mount_point:

#         for folder,items in things:
#             for i,row in items.iterrows():
#                 if not os.path.isdir(folder):
#                     pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
#                     pass
#                 target = f"{folder}/{row.filename}"
#                 os.link(row.original_path, target)


# def buz(i):
#     if i>0:
#         p = "|     "*(i-1)
#         e = "|--"
#         return f"{p}{e}"
#     elif i==0:
#         return ""
#     else:
#         return "tHe fUck is tHiS?>"




def mount_paths(original, target):
    assert len(original) == len(target),"list of original and target paths need to match in lenth"
    import pathlib

    for orig,targ in zip(original,target):
        # folder,file = os.path.split(orig)
        # mounted_folder = f"{mountroot}{folder}"
        # mounted_file = f"{mountroot}{path}"

        mount_folder,mount_file = os.path.split(targ)
        pathlib.Path(mount_folder).mkdir(parents=True, exist_ok=True)

        # print(f"linking {orig} to {targ}")
        os.symlink(orig, targ)


# def foo( groups ,by):
#     l = []
#     for gn,g in groups:
#         for ggn,gg in g.groupby(by):
# #             print(f">>{gn}/{ggn}:{len(gg)}")
#             l.append((f"{gn}/{ggn}",gg))
#     return l

def mount(args, config_object,df):
    assert args.mountby, "You need to choose how to mount the files with the --mountby option"
    import pathlib
    
    mountroot = os.path.join(os.path.abspath(args.mountpoint),"dito_virtual_fs")
    # if the folder exists, make a new one inside it
    if os.path.isdir(os.path.abspath(args.mountpoint)):
        mountroot = os.path.abspath(os.path.join(args.mountpoint,"dito_virtual_fs"))

    else:
        # if not, we'll create this new folder as asked
        mountroot = args.mountpoint



    if args.mountby=='paths':
        assert 'path' in df.columns, "This table doesn't contain paths"
        
        original_paths = df.path.values

        if args.lefttrimpath:
            broken_paths = [x.split('/') for x in df.path.values]
            while 1:
                if len(set([x[0] for x in broken_paths]))==1:
                    broken_paths = [x[1:] for x in broken_paths]
                else:
                    break
            joined_paths = ["/".join(x) for x in broken_paths]
            target_paths = [os.path.abspath(f"{mountroot}/{p}") for p in joined_paths]
        else:
            target_paths = [os.path.abspath(f"{mountroot}/{p}") for p in df.path.values]
        # print(target_paths)
        mount_paths(original_paths,target_paths )
    
    else:
        if 'rep' not in df['replicate'].values[0]:
            df['replicate'] = df['replicate'].apply(lambda x: f"repl{x}")

        if 'path' in df.columns:
            original_paths = df.path.values            
        else:
            # if there's no path in the dataframe, we can link to the hardlink backup
            # of the file. That's a file inside basedir/hardlinks which is hardlinked
            # to the original file with this checksum (ghash)
            basedir = config_object['DEFAULT']['BASEDIR']
            df['path'] = df['ghash'].apply(lambda h: f"{basedir}/hardlinks/{h}" )


        df['tfolder'] = df[args.mountby].apply(lambda r: "/" + "/".join(r.values) , axis=1)
        df['folder'] = df.tfolder.apply(lambda t: os.path.abspath(f"{mountroot}{t}") )

        if not args.infilename:
            log.warning("You should specify which attrs to include in the filenames. By default we're using: organism, stage,technique,replicate,filenum")
            args.infilename = ["organism", "stage","technique","replicate","filenum"]
        df['tname'] = df[args.infilename].apply(lambda r: "_".join(r.values) + ".fastq.gz", axis=1)
        df['target_paths'] = df[['folder','tname']].apply(lambda r: "/".join(r.values) ,axis=1)
        
        mount_paths(original_paths,df.target_paths.values )



def notnan(s):
    if s not in ["NotSet","NaN",None, np.NaN,0,0.]:
        if s==s:
            return True
    return False


    
def locdf(df, args):
    if args.loc:
        loc = args.loc
        if "|" in loc:
            s1,s2 = args.loc.split("|")
        else:
            s1=":"
            s2=loc
            
        foo = lambda x: int(x) if x else None
        a,b = [foo(x) for x in s1.split(":")]
        slice1 = slice(a,b)

        if ',' in s2:
            slice2 = s2.split(",")
        elif ":" in s2:
            c,d = slice2.split(":")
            slice2 = slice(c,d)
        else:
            slice2 = [s2]

        df = df.loc[slice1,slice2]
        return df

    else:
        return df


def filterdf(df, argstring):
    dfc = df.copy()
    rules = [x.split(":") for x in argstring]
    for column,target in rules:
        # in the case of the path, we'll use 'startwith'
        # instead of ==
        if column=='path':
            dfc = dfc.loc[dfc[column].str.contains(target)]
            continue
        if column in ['exists','n_links']:
            dfc = dfc.loc[dfc[column].astype(int)==int(target)]
            continue


        dfc = dfc.loc[dfc[column]==target]
    return dfc











import re

class PathRegexer(object):
    # just a handly little class, holds the code for testing one path with a number
    # of positive or negative (v) regular expressions
    def __init__(self,regex,regexv):
        if regex:
            self.regexes = [re.compile(r) for r in regex]
        else:
            self.regexes=None
        if regexv:
            self.regexves = [re.compile(r) for r in regexv]
        else:
            self.regexves = None

    def baz(self,p):
        # p that matches all regexes passes
        if self.regexes:
            for r in self.regexes:
                if not re.match(r,p):
                    return False
            else:
                return True
        else:
            return True
    def nbaz(self,p):
        # p that matches any regexv is removed
        if self.regexves:
            for r in self.regexves:
                if re.match(r,p):
                    return False
            else:
                return True
        else:
            return True
    def test(self,path):
        # print(f"{path} : {self.baz(path)} {self.nbaz(path)}")
        return self.baz(path) and self.nbaz(path)


    # def inpath(self, tests, path):
    #     for t in tests:
    #         if t in path:
    #             return True
    #     else:
    #         return False

    # def notinpath(self, tests, path):
    #     for t in tests:
    #         if t in path:
    #             return False
    #     else:
    #         return True













