# Holds some regexes to extract information from file paths


# no assumption yet for these
# assumed_treatment
# assumed_target
# assumed_filenum

# assumed_owner
# assumed_extension
# some assumptions for these
# assumed_tissue
# assumed_replicate
# assumed_stage
# assumed_organism
# assumed_technique

# possible_values = [x[0].split("_")[1] for x in token_specification]

from . import const
import logging 
log = logging.getLogger('dito')

token_specification = [
        ('organism_Blanceolatum', r'amphioxus|bralan|[-_/]+amphi[-_/]+|anphioxus|bla|amphiox'),  
        ('organism_Drerio', r'zebra|drer|danrer|d[_-]*rerio'),
        ('organism_Mmusculus', r'mouse|mmu|mm10|mm9'),
        ('organism_AmexicanusPachon', r'cavefish'),
        ('organism_AmexicanusSurface', r'surface'),
        ('organism_Olatipes', r'medaka'),
        ('organism_Pflava', r'Pfla_2015'),
        ('organism_Hsapiens', r'human|hg19|gm12878'),
        ('organism_skate', r'skate'),
        ('organism_turbot', r'turbot'),
        ('organism_Xtropicalis', r'xentro|xtrop|xtro|xtropicalis'),
        ('organism_girasol', r'girasol'),
        ('organism_catshark', r'catshark'),
        ('organism_Pmammillata', r'Phallusia_mammillata|ph_mamm'),
        ('organism_Crobusta', r'c_robusta'),
        ('organism_Pminiata', r'p_miniata'),
        ('organism_Plividus', r'Plividus'),
        ('organism_Spurpuratus', r's_purpuratus|spur5_new'),
        ('organism_Dmauritania', r'Dmauritania'),
        ('organism_Dmelanogaster', r'Dmelanogaster'),
        ('organism_Dsimulans', r'Dsimulans'),
        ('organism_Ggalus', r'chicken'),
        ('organism_Capsaspora', r'capsaspora'),
        ('organism_Ciona', r'ciona'),
    
    
        ('technique_ATACseq', r'atac'),
        ('technique_HICHIPseq', r'hi.*chip'),
        ('technique_HICseq', r'hi.*c[^a-zA-Z]'),
        ('technique_CHIPseq', r'chip'),
        ('technique_RNAseq', r'rna'),
        ('technique_4Cseq', r'[^a-zA-Z]4c[^a-zA-Z]'),
        ('technique_CAGEseq', r'CAGE'),
    
    
    
        ('stage_80epi', r'80epi|80epibolia'),
        ('stage_hpf', r'([0-9]+)h[pf]*[.\-_]+'),
        ('stage_dph', r'[\\\-_/][0-9]+dph[.\-_/]+'),
        ('stage_E', r'[\-_]+E\d+\.*\d*[\-_]+'),
    
    
    
        ('replicate_rep', r'[^a-zA-Z]rep([0-9]+)[^a-zA-Z]'),
        ('replicate_repl', r'[^a-zA-Z]replicate([0-9]+)[^a-zA-Z]'),
    
        ('tissue_pancreas', r'pancreas'),
        ('tissue_liver', r'liver'),
        ('tissue_retina', r'retina'),
        ('tissue_heart', r'heart'),
        ('tissue_kidney', r'kidney'),
        ('tissue_hindbrain', r'hindbrain'),
    ]


# to be imported by other code:
possible_values = {}
for x in token_specification:
    k,v = x[0].split("_")
    if (k not in possible_values):
        if ('stage'==k):
            continue
        possible_values[k] = []
    possible_values[k].append(v)
################################

tok_regex = '|'.join(f'(?P<{k}>{v})' for k,v in token_specification)
import re, os
import pathlib


from functools import partial

def path_assumptions(path, vprintv=0):
        
    filed = {}
    log.debug(f"Assuming attributes for {path}")    

    for mo in re.finditer(tok_regex, path,flags=re.IGNORECASE):
        k,v = mo.lastgroup.split("_")
        

        if k.startswith("replicate"):
            v="rep{}".format( re.search('[0-9]+', mo.group()).group() )
        if "stage"==k and "hpf"==v:
            v = re.search('[0-9]+', mo.group()).group()
        if "stage"==k and "E"==v:
            v = mo.group().replace("_","").replace("-","")
        # if "stage"==k and "dph"==v:
        #     v = "{}dph".format(re.search('[0-9]+', mo.group()).group())

        if k not in filed:
            log.debug(f"Found key {k} with value {v}")    
            filed[f"{k}"] = "<MULTI>".join(list(set([v])))
            # print("<MULTI>".join(list(set([v]))))
    

    try:
        filed['owner'] = os.path.normpath(path).split(os.sep)[3]
    except:
        filed['owner'] = None

    path = pathlib.Path(path)
    try:
        file_extension = "".join(path.suffixes)
        filed['extension'] = file_extension
    except:
        filed['extension'] = None


    return filed