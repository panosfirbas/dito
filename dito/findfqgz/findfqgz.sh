#!/bin/bash

find ${1} -type f ! -type l  -regex  '.*\(.fq.gz\|.fastq.gz\)$'  | grep -vFf excludes.txt | ./filter_bad_symlinks.sh