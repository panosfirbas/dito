#!/bin/bash

./findfqgz.sh /home/ska | \
rmlint -x --keep-hardlinked -t 8 \
-o summary:stdout -o pretty:stdout -o json:bigrmlintrun.json -o csv:bigrmlintrun.csv -o uniques:bigrmlintUniques.txt \
-c sh:hardlink -o sh:bigrmlintrun.sh \
-S d'r<^/home/ska/jtena/.*$>''R<^/home/ska/alvaro.*$>''R<^/home/ska/sjimgan.*$>'Oma -