

class AD(dict):
    def __init__(self, *args, **kwargs):
        super(AD, self).__init__(*args, **kwargs)
        self.__dict__ = self

es = "                                                                                            "
d = AD({
	'dito' : AD({
			'description': "Dito is a data management tool that helps us organize our fastq files.\
						    You can use it to discover files that are already in the system (dito ls),\
						    to add files to the system (dito scan), or to annotate files (dito annotate).\
						    If you are an administrator, you can run rmlint reports easily, manage dito's database,\
						    and handle backup to our NAS. Make sure to also read the README in https://gitlab.com/panosfirbas/dito"
				}),
	'scanner': AD({
		'description' : "This function starts at a path of your system and recursively looks for fastq.gz files inside that path.\
						Files that dito hadn't seen before will be added to the database, make sure to annotate them later with 'dito annotate'.\
						If you are not the owner of any files in this path (meaning dito cannot read them), they will be ignored."
				}),

	'rml' : AD({
			'description' : "This module handles rmlint reports. Rmlint is a tool that discovers duplicated\
					        files in your system. We run a specific rmlint command, designed around our system to discover\
					        .fastq.gz files. Those files are then stored in dito's database.\n\n\
					        Each report generates a number of files, all stored in a folder made for that report.",
		    'digest' : AD({

			    'description': "This reads a report's results\
						        and adds newly discovered files in dito's database. Dito computes the checksums of some files in order to deduplicate\
						        as much as possible, so this command might take a long time to finish, depending on how many files need to be checksumed.\n\n\
						        You need to specify the number of the report, use 'dito rml ls' to list the available reports."
		    	}),
		    'generate' : AD({
		    	"description" : "This can take many hours to finish, consider starting it in a (linux)screen so it won't be interrupted."})
			}),
	'mount': AD({
		"description" : "Dito allows you to mount files in a meaningful folder structure wherever you want. You can choose to keep \
		their original path by choosing the -by path option, or by attributes such as organism/technique by using the -by option and \
		specifying a list of attributes. For example -by 'organism,tissue,stage'. "
		}),
	'annotate' : AD({
		"description" : "This function finds paths in dito's database that are downstream of your CurrentWorkingDirectory\
        (i.e. if you are in your home, it'll consider only files found in your home) and lets you annotate them.\
        When you use the --commit option, all displayed files will \
        be annotated with your options. Filter your files with regular expressions until you only see the ones you want to annotate, set your annotation and --commit.\
        Use the --do (downstream only) and regular expression filters to select a number of files by their path. Then,\
		annotate them by, for example calling '--organism Mmusculus'. The help menu shows you previously used values, so please consider \
		those in order to not have different names for the same organism/tissue/etc. The command does not commit any changes unless you \
		pass the --commit option, so you can call the command and examine its output to figure things out and then add the --commit to .. commit."
		}),
	'tables' : AD({
		"description"  : "Dito keeps track of files based on their checksum. It annotates each checksum by combining annotations of potentially\
		multiple different paths (that nevertheless point to the same data), in the 'reasonable knowledge' table. By default, the 'ls' function\
		works on this high order table. Use the filtering arguments to control which files to display, the display arguments to control how \
		the table is shown and the mounting arguments if you want to mount the resulting files somewhere in your system. You can print the resulting table in a csv file with the \
		--csv option. The --examples option shows some usage examples.\
		For more advanced uses of this command (to see the underlying tables), please read the documentation on dito's repository: https://gitlab.com/panosfirbas/dito",
		"examples" : 
"#print the entire 'files' table:\n\
dito ls -t files\n\n\
#print the entire 'files' table in a csv file:\n\
dito ls -t files --to-csv ./filestable.csv\n\n\
#only print the path and ghash columns of the files table\n\
dito ls -t files -loc \"path,ghash\"\n\n\
#filter the files table by its path\n\
dito ls -t files -path \"/home/ska/panos\"\n\n\
#print the paths that dito annotated as Hsapiens\n\
#they are found in the assumed table, so we choose that \n\
dito ls -t assumed -f \"organism:Hsapiens\"\n\n\
",

		"print" : AD({
			"description" : 
f'Printing these tables in the command line is not very practical. To reduce \n\
the amount of information that needs to be displayed, specify which table you\n\
want to see, and filter the row/columns.\n\
\n\n\
Filtering works by providing a string of column:option pairs separated by commas.\n\
For example, -f "path:/home/ska/panos" will keep only the paths that contain \n\
/home/ska/panos, effectively showing only the files belonging to panos.\n\
-f "path:/home/ska/panos,exists:1" will further filter paths that currently \n\
exist on the disk.\n\
The -loc option filters the rows and columns of the table by passing a string\n\
with the following format: "row_filter|column_filter". If there is no "|" \n\
in your string, the whole string will be treated as a column filter. The \n\
column filter is a list of comma separated column names. For example,\n\
dito tables print -t files -f "path:/home/ska/panos" -loc "path,inode,size"\n\
will only only show the path,inode,size columns of the files table\n\
-loc "5:100|path,inode,size" will show rows 5 to 100.'


			})

		})
	})