from . import load_configuration
from . import cli
from . import loggers
from .store import MyDBO

import os,sys

import logging



def main():    

    # load the configuration object, just a little
    # dictionary-like object to keep track of some paths
    # like where the database is
    config = load_configuration.main()


    # we call the main function of the command line script
    # and get the arguments (args) object back
    args = cli.main(config)

    # load the logging configuration
    # in there we setup some loggers
    # we don't need to pass them, they are accessible through the logging 
    # module
    loggers.main(args,config)

    # Here, we instantiate a database object
    # this way, whenever that object falls out of scope, it's destructor is called
    # and the db connection closes elegantly
    # we pass this db object through our config object
    instantiated_db_connection = MyDBO(config)
    config.dbo = instantiated_db_connection

    try:
        # We have stored a function as the 'main' function in the arguments
        # Here we attempt to execute it
        args.func(args, config)
    except AttributeError:
        import traceback
        traceback.print_exc()
        # if no default function is set (when the user specifies a subparser but no command for example)
        # append the help in the arguments and parse them again
        # this forces it to display the usage of the subparser
        a = sys.argv[1:] + ["--help"]
        p.parse_args(a)


    # closing down stuff
    # make sure the db file is rw for all
    # TODO should also make sure all hardlinks belong to root and are read only
    try:
        os.chmod(config.dbpath, 0o777)
    except PermissionError:
        pass


if __name__ == '__main__':
    main()