





# Investigate the reports
=================================================

Rmlint reports are kept in ${basedir}/rml_reports/ . In there, we find a folder for each
report that was generated. To list all available reports, run     
       
       >> dito rml ls

This lists the full paths of available reports, to make the next step easy:     
       
       >> ls /the/path/to/a/rml/report_1_somedate/

In a report's folder, we'll find:       
          
> ocsv.csv
The report's data in csv format

> ojson.json
The report's data in json format. Only duplicated files are reported here. Hardlinks are
reported, softlinks no. We use this file to import files with digest report

> ooriginals.originals
We generate this from the json file, just a list of 'original' files. We have duplicates
of these, but these were chosen as original. We don't use that information later.

> osh.sh
The report's bash script, run this to make the hardlinks suggested by rmlint

> ostdout.stdout
The stdout output of the report


> ouniques.uniques
This is a list of filepaths, no duplicates where found for these.







# Generate a new rmlint report
=================================================

To make a new report, run :

    >> dito rml generate_new          

         
You will need sudo access for this, because rmlint needs to read the files in everyone's 
home (since it's configured to run on /home/ska ). A new folder will be created in the 
rml_reports folder, with a new id and a timestamp.         
          



# Digesting a report
=================================================

After running an rmlint report, we need to digest it with dito. Dito reads rmlint's report
and makes records of new files.        
           
    # to digest the report number 1:
    >> dito rml digest --n 1

This might take sume time to finish.



# After digesting a report
=================================================

We should now update some of our tables:       
          
    # The following command, updated the 'assumed attributes' table
    # that is, the attributes we can extract from the pahts of the files
    >>dito tables update_assumptions

    # Next, we need to update our reasonable_knowledge table
    >>dito tables update_rk