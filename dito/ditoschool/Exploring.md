






Ideally, you would like to search for data by their attributes, for example:

# let's find all the zebrafish experiments
# we'll explore the 'rk (reasonable knowledge)' table

>> dito tables print -t rk -mr 10 -mcw 100 -f organism:Drerio

# now only the chipseq experiments
>> dito tables print -t rk -mr 10 -mcw 100 -f organism:Drerio,technique:CHIPseq

# in a specific stage
>> dito tables print -t rk -mr 10 -mcw 100 -f organism:Drerio,technique:CHIPseq,stage:24h



If these were the experiments you were looking for, dito can mount them for you, in a 
meaningful way.