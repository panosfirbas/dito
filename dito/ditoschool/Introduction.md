# What is dito
=================================================

Dito is a little tool that helps us organize important files. At the momment it only
concerns itself with .fastq.gz files. It spawns rmlint scans to look for fastq files in the 
system (sudo needed to access everyone's files) and then digests the rmlint reports to
keep records of the fastq files. The records are kept (with python) in a hdf object that
acts as a quasi-database. This object can be very quickly loaded into a dataframe. Copies
of the table are kept as .csv.

Dito facilitates the exploration of the database, the annotation of files with regards to
their organism, technique etc, allows mounting groups of files to quickly bootstrap new 
projects, and finally calls backup scripts to facilitate backing up our files in a different
filesystem.

             
             
             
# Installation information
=================================================                
      
Dito infers its installation directory. In there, it stores a little configuration file,
where it keeps a basedir path. If that is not set, you will be prompted to choose one. This
is a path to a folder for dito to store its stuff, something like /home/ska/dito.      
        
In the basedir folder, we'll find 'dbtable.h5', that's our database object.      
extra_options.ini is another small configuration file where we can edit some options manually.       
rml_reports is a folder where rmlint report are kept. THose are tagged by an increasing 
number 'n' and the timestamp of creation.                
               
             
             

# The tables
=================================================   


## The files table
=================================================   
Each path (to a file) is associated and indexed by its checksum. Multiple maths might be
associated to the same checksum (checksum of gziped file, ghash) (when they are copies 
of eachother) and should be changed to hardlinks (rmlint.sh should handle that). If a 
fastq file is gzipped two different times, the resulting .fastq.gz files will produce 
different checksums, because gzip stores info in the header of the .gz. 
Because of that, the true checksum for our files (thash) is the checksum of the unzipped
contents of a file. Files with the same ghash will produce the same thash, but different
ghashes might point to the same thash.         
          
The first table of our database (files table), associates paths to their checksums and
some filesystem information.        

       # To see the head of this table, run         
       >> dito tables print -t files -mr 10

       # Change the max col width to see more of the path/hash columns
       >> dito tables print -t files -mr 10 -mwc 50

       # to only print the thash,ghash,path columns, and show even more of them
       >> dito tables print -t files -mr 10 -mcw 100 -loc ":|thash,ghash,path"


> thash 

The checksum of the unzipped folder's contents. Only computed when needed, else assigned
a unique id.
          
> ghash       
         
The checksum of the zipped file. If no putative copies have been encountered, no reason to 
compute. Might get inherited by the rmlint report (if inode info agrees, therefore we know
the file rmlint saw and the current system file are the same).
          
> path      
            
The path of the file

> size      
          
The size of the file on disk.        
           
> inode             
            
The inode of the file. Paths in a linux filesystem are just names that point to a location
on the disk. The inode value is a unique index value, pointing to the location of the disk
where the data of the path reside. Hardlinks point to the same inode. If the inode of a path
changes, it means that the underlying data of the file have changed too.
            
> disk_id          
                
The id of the disk on which the file exists. Just to make sure we're not talking about
inodes on two different disks. Every file in our system should have the same value here.      
                 
> mtime              
                  
Time of last modification. Like disk_id and inode, helps us make sure the file is unchanged            
            
> n_links                 
                 
Harlinks have values >1 here.          
              
> exists               

This row (path, inode info etc) currently exists in the filesystem. Old paths will be kept
since they might be carrying annotation information that we don't want to lose.         
              
## Assumed attributes         
=================================================
            
We keep track of a number of attributes per filepath. Some attributes can be assumed from
the path itself. We compute those and store them in the 'assumed' table. Its columns are:          
           
    path owner organism tissue stage treatment technique target replicate filenum extension           


    # To see the head of this table, run         
    >> dito tables print -t assumed -mr 10 -mwc 100


## Manually set attributes         
=================================================
           
Some attributes cannot be assumed and in anycase, manual annotation is always best. For this
we keep a separate table where we store manual attributes per filepath. Its columns are:         
            
    path organism tissue stage treatment technique target replicate filenum skip done                     


    # To see the head of this table, run         
    >> dito tables print -t manual -mr 10 -mwc 100

## The 'reasonable knowledge table'         
=================================================
             
Image we have two copies of a file. One in userA's home and one in userB's home. If userA       
manually annotates the file, the annotation should also apply to userB's file. To do this,       
we keep a 'reasonable knowledge' table. The values of this table we updated from values
in the 'assumed' and 'manual' tables. For each unique 'thash', we look at all filepaths
that are associated to it, and extract assumed or manual annotations. If we have manual
annotation for one of the paths of a 'thash', we keep that as our best knowledge for this
file. Once we calculate the reasonable knowledge per thash, we can easily apply that to
all files. The 'rkp' table is produced by merging the path/thash pairs of the files table,
with the best annotations per thash from the 'rk' table, giving us the best possible  
annotation per filepath.

    
    # To see the top of this table:
    >> dito tables print -t rk -mr 10

    # The rkp table, prints paths and their associated value from the rk table
    >> dito tables print -t rkp -mr 10

The quality column is a metric of how good our annotation is. By marking a path's annotation
as 'done' we confirm that no more annotation is needed for this.



                 
