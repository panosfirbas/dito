









To reiterate what was said in the Introduction.md, dito keeps a row in two tables for each
path. The one is the 'assumed' table, where we keep attributes automatically generated
from the path itself (dito tables print -t assumed -mr 10 -mwc 100). The other is the 
'manual' table, where we keep attributes that were manually set (dito tables print -t manual -mr 10 -mwc 100)           
          

Those two tables are combined into the 'reasonable knowledge' table, where for each unique
checksum, we get the best available attributes from all of its paths. This way we don't 
need to annotate something manually twice.       
        

# How to manually annotate       
=============================================

First, we should look for files that lack annotation.
                           
    # the rkp table, is a merge of filepaths:their assigned rk table row
    # rkpi is a shortcup to only show the more importabt rkp columns
    # we set a maxcolwith to 100 and sort_ascending by quality then path
    # This will show us the top 10 files with the worst annotation
    >>dito tables print -t rkpi -mr 10 -mcw 100 -sa quality,path

Now, let's say we identified a folder with a number of files that need annotation,
maybe '/home/ska/panos/boxes/o_p63/data/raw/'.       
             
We can enter that folder, and run 'dito annotate --do'       

    >>cd /home/ska/panos/boxes/o_p63/data/raw/
    
    # This will tell dito to annotate downstream-only
    # Dito will look for files in its database that are downstream of your current path
    # show you their current annotation
    # then apply your annotation (which is none for now)
    # and show you the updated table.
    # Unless you pass the --commit option, no changes will be stored, so you can 
    # freely test things out
    >>dito annotate --do


The last command, printed only the files that were downstream of us in the filesystem.    
We make sure that only the files we want to annotate appear here. 
We can further narrow down our selection, by applying regular expression filters:      
      
    #  for example, to only work with _1 files (the first file of a paired-end sequencing)
    # we can run the following
    # keep in mind this is a regular expression, so you need ".*" for "anything"
    # and you need to escape dots ("\.fq" not just ".fq") and other important characters
    >>dito annotate --do --regex ".*_1\.fq\.gz"

Once we have selected the files on which we mean to apply our annotation, we can pass
more options.       

    # mark the _1 files as filenum 1
    >> dito annotate --do --regex ".*_1\.fq\.gz" --filenum 1 

    # Remember to commit to apply the changes
    >> dito annotate --do --regex ".*_1\.fq\.gz" --filenum 1 --commit

    # Reverse the regex filter, and mark the _2 files
    >> dito annotate --do --regexv ".*_1\.fq\.gz" --filenum 2 --commit

    # Mark replicate 1
    >> dito annotate --do --regex ".*_rep1" --replicate 1 --commit

    # Mark replicate 2
    >> dito annotate --do --regexv ".*_rep1" --replicate 2 --commit

    # Let's see what organisms have been used before
    >> dito annotate --help
    # This will print the help menu of annotate, which dynamically shows you 
    # all the unique values that have been used for each attribute.
    # Make sure to check this in order to minimize redundancy.

    # we can add the organism info etc for all the files, no regex filters needed
    # We mark them as done, no more annotation expected
    >>dito annotate --do --organism Drerio --tissue while_embryo -st 24h -tec CHIPseq --done 

    #remember to --commit

