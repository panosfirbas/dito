# Handles rmlint report logic

import sys
import argparse
import os
import pandas as pd
import numpy as np

import glob
from functools import partial

from . import const
from . import store as storelib


import uuid


# this script holds code relevant to rmlint reports, spawning or digesting



import logging 
log = logging.getLogger('dito')





def digest_report(args, config_object):
    """Takes a number, finds the appropriate report and digests the results"""
    # find the report folder
    # the reports are stored in rml_reports folder in our basedir
    # remember, we loaded the basdir from the configuration loading
    # and the rml_reports folder is hardcoded
    reportsdir = os.path.join(config_object['DEFAULT']['basedir'],"rml_reports")
    # the report folders have a pattern of report_X_timestamp
    # where X is a unique number
    ss=f"{reportsdir}/report_{args.n}*"
    globresults = list(glob.glob(ss))
    # there should be only one result
    assert len(globresults)==1, 'More than one reports with the same number found, WEIRD!'
    report_path = globresults[0]

    # # make sure the database is fine
    # dbpath = config_object['DEFAULT']['dbname']
    # BASEDIR=config_object['DEFAULT'].get('basedir',None)
    # dbpath = os.path.join(BASEDIR,dbpath)
    # assert os.path.isfile(dbpath), 'database not found from digest report'

    # pseudocode
    fs = Filescanner(config_object)    
    # for path in fs.scanpath(f"{args.path}"):
    #     fs.digest_fp(path)
    
    

    # block
    # Digest uniques:   
    uniquespath = os.path.join(report_path,"ouniques.uniques")
    # c=0
    # with open(uniquespath,'r') as fi:
    #     for line in fi:
    #         c+=1
    # vprint(1,f"theres {c} lines in the uniques file")
    with open(uniquespath, 'r') as fi:
        lot = []
        for en,path in enumerate(fi):
            how,drow = fs.digest_fp(path)
    # endblock

    # Digest the json:   
    jsonpath = os.path.join(report_path,"ojson.json")
    
    lot = json.load(open(jsonpath,'r'))[1:-1]
    for d in lot:
        fs.digest_fp(d['path'])

    # store = pd.HDFStore(dbpath)
    # store.close()








def generate_report(args, config_object):
    original_path = os.getcwd()

    # in the isntall dir, we have a folder holding relevant bash scripts
    # we enter in there to call them in the command line
    code_root = os.path.join(config_object['DEFAULT']['installdir'], "findfqgz")
    # print(list(config_object['DEFAULT'].items()))
    log.info("Generating an rmlint report")
    os.chdir(code_root)

    import tempfile
    tmp = tempfile.NamedTemporaryFile()

    rmlroot = args.root

    # HANDLE A NEW DIRECTORY FOR THE NEW REPORT
    reportsdir = os.path.join(config_object['DEFAULT']['basedir'],"rml_reports")
    if not os.path.isdir(reportsdir):
        os.mkdir(reportsdir)
    import re
    dirs = [d for d in os.listdir(reportsdir) if os.path.isdir(os.path.join(reportsdir, d))]
    if len(dirs)>0:
        reg = re.compile("^report_(\d*)_")
        nums = [int(reg.search(ddir).group(1)) for ddir in dirs ]
        maxreportnum = max(nums) 
    else:
        maxreportnum = 0
    import datetime
    nowstring = datetime.datetime.now().strftime("%Y-%m-%d:%H:%M")
    thisreportsdir = os.path.join(reportsdir,f"report_{maxreportnum+1}_{nowstring}")
    if not os.path.isdir(thisreportsdir):
        log.info(f"Saving report in {thisreportsdir}")
        os.mkdir(thisreportsdir)
    else:
        raise "errordddd"


    # setup some names for the generated files
    ojson = os.path.join(thisreportsdir,"ojson.json")
    ocsv = os.path.join(thisreportsdir,"ocsv.csv")
    ostdout = os.path.join(thisreportsdir,"ostdout.stdout")
    ouniques = os.path.join(thisreportsdir,"ouniques.uniques")
    osh = os.path.join(thisreportsdir,"osh.sh")
    originals = os.path.join(thisreportsdir,"ooriginals.originals")


    # HERE IS THE COMPLETE RMLINT COMMAND #TODO move to configuration ?
    import subprocess
#-o pretty:{ostdout} 
    # construct the string
    # the start
    rmlc  = f"./findfqgz.sh {rmlroot} | "
    # -x to stay on the same disk
    # --hardlinked reports hardlinks to us so we include them in the db
    # -t to use 8 cpus
    # -C to use the xattrs. This makes subsequent runs much faster. Needs root
    rmlc += f"rmlint -x --hardlinked -t 8 -C " 
    rmlc += f"-o summary:{ostdout} -o json:{ojson} -o csv:{ocsv} -o uniques:{ouniques} "
    rmlc += f"-c sh:hardlink -o sh:{osh} "
    rmlc += f"-S d'r<^/home/ska/jtena/.*$>''R<^/home/ska/alvaro.*$>''R<^/home/ska/sjimgan.*$>'Oma - // /home/ska/panos/foodir"

    log.info(f"This is the full rmlint command issued: {rmlc}")
    # throw the string in bash and get results
    process = subprocess.check_output(rmlc,shell=True)  

    # with this oneliner, we extract the originals from the json output
    # those, and the uniques are the ones we should digest first
    getoriginalscom = f"cat {ojson} | jq -r '.[1:-1][] | select(.is_original) | .path' | sort > {originals}"
    process = subprocess.check_output(getoriginalscom,shell=True)  

    # go back where we were
    os.chdir(original_path)

def ls_report(args, config_object):
    reportsdir = os.path.join(config_object['DEFAULT']['basedir'],"rml_reports")
    # the report folders have a pattern of report_X_timestamp
    # where X is a unique number
    ss=f"{reportsdir}/report_*"
    globresults = list(glob.glob(ss))
    for r in globresults:
        print(r)
    # there should be only one result

def main():
    
    # config = get_config_object(INSTALDIR) # load configuration options from ini files.
    # return config
    return 1


if __name__ == '__main__':
    main()