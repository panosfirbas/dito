

# Theory material, read these:
#  http://www.mikerubel.org/computers/rsync_snapshots/#Incremental
#  https://sites.google.com/site/rsync2u/home/rsync-tutorial/how-rsync-works3
###################


# the nas server 
# admin@192.168.49.95
NAS=${1}

# where on the nas we store these backups
# /share/CACHEDEV1_DATA/Backups/nodos/
# in there, we'll have all our backup.0, backup.1,backup.2 etc folders
nasBACKUP_DIR=${2}


# ssh into nas and roll backups up (2->3, 1->2, 0->1 )
echo 'Executing the first step on NAS'
ssh ${NAS} 'bash -s' < nas_roll_up.sh $nasBACKUP_DIR
# this little snippet checks if the previous command worked properly
if [ $? -eq 0 ]; then
    echo Success!

	# construct the path for the backup and give it to the next script which does the syncing
	BK_PATH=${NAS}:${nasBACKUP_DIR}

	# where to keep the rsync logs
	# /home/ska/dito/logs
	LOGDIR=${3}


	# We don't copy the entire system, instead we give rsync a curated list of files
	# this should be created by dito with a find command
	# and should include everything in dito's base dir (which contains the folder with the hardlinks)
	FILE_LIST=${4}

	echo Executing the rsync command with backup path: ${BK_PATH} and logdir: ${LOGDIR}
	bash rsync_command.sh ${BK_PATH} ${FILE_LIST} ${LOGDIR}
	sleep 5
	# this little snippet checks if the previous command worked properly
	if [ $? -eq 0 ]; then
	    echo Success!
	else
	    echo FAIL in the rsync!!
	    # run this to roll the backups back down !
	    # we lost the older backup but we can try again
	    ssh ${NAS} 'bash -s' < nas_roll_down.sh $nasBACKUP_DIR

	    exit 1
	fi

else
	# the rollup failed
    echo FAIL!
    exit 1
fi


