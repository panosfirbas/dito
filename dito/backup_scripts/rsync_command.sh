





# The remote destination of the backup
# In a previous step, we have remotely sshed in the nas
# and moved the previous bk.0 to bk.1
# so this command will create a new folder
BK_PATH=${1}
NEW_BK_DIR=${BK_PATH}/backup.0
# But we'll be using the --link-dest option
# which makes rsync to look into a directory (backup.1) and if on of the files
# we're trying to copy to backup.0 exists in backup.1, then we make a hardlink
# to the file in backup.1 instead
# The link-dest directory is relative to the new_bk_dir, see here:
# https://serverfault.com/questions/627629/rsync-link-destination-remote-server
PREVIOUS_BK_DIR=../backup.1

FILE_LIST=${2}
LOGDIR=${3}

current_time=$(date "+%d.%m.%Y")

out=${LOGDIR}/rsync_log${current_time}.stdout
err=${LOGDIR}/rsync_log${current_time}.stderr

# relative means it stores files with their full path inside the destination
# so in nas we have things like /path/to/backup.0/home/ska/panos/my/folders
# links keeps symbolic links as symbolic links
# hard-links keeps hardlinks !important
# times keeps modification times !important
# files from is a file containing the list of files to sync !important
# link dest tells rsync to hardlink to files found in backup1 if they exist unchanged 
# the fancy >>() things at the end redirect the standard out and err into files for logging
rsync --verbose --relative --links --hard-links --times --files-from=$FILE_LIST --link-dest=${PREVIOUS_BK_DIR} / ${NEW_BK_DIR} > >(tee -a ${out} ) 2> >(tee -a ${err} >&2)
