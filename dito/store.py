# Handles database logic


from . import const

import sys
import argparse
import os
import pandas as pd
import numpy as np
import glob
from functools import partial

import logging 
log = logging.getLogger('dito')

from . import regex
import numpy as np
import shutil

# storelib.dptable_getinit(config_object.dbpath, "files")









# def migrate(args, config_object):
#     """If the design of the database, as defined in .const.py, has changed,
#     try to migrate the old table to the new design."""

#     # have things changed?
    
#     dbpath = config_object.dbpath
#     # get definition
#     for definition_table in const.tables:
#         # get db table
#         df = dptable_getinit(dbpath, definition_table.key)
#         for column in definition_table.TABLECOLS:
#             if not column in df:
#                 print(f"In table {definition_table.key}, the defined column {column} does not exist in database")
#                 # i need to add a new column to the db table
#                 assert definition_table.default_rowD[column], f"No default value found for column {column}, cannot initialize database"

#                 try:
#                     # make a new column in the dataframe, with the default value of the definition
#                     df[column] = definition_table.default_rowD[column]
#                     # give the correct dtype to the column
#                     df[column] = df[column].astype(definition_table.TABLECOLS_dtypesD[column])
#                     # save the changes
#                     dptable_safeput(dbpath, definition_table.key, df)
#                     print(f"In table {definition_table.key}, the defined column {column} was added to the database with default values")
#                 except:
#                     print(f"Failed to add column to the database")

#             # if df[column].dtype != definition_table.TABLECOLS_dtypesD[column]:
#             #     print(f"Database column {column} has different dtype than the definition. Database is {df[column].dtype}, definition is {definition_table.TABLECOLS_dtypesD[column]}")


#         # now the other way around, should we delete anything?
#         for column in df:
#             if not column in definition_table.TABLECOLS:
#                 print(f"In table {definition_table.key}, the database column {column} does not exist in definition")









# def putkv(key,k,v,dbpath):

#     assert len(k)==2
#     i,j = k

#     df = dptable_getinit(dbpath, key)
#     df.loc[i,j] = v

#     dptable_safeput(dbpath, key, df)

    


# def update_store_df_rows_col(dbpath,key, i_s,col, newrowvalues, data_columns):
#     # dbpath = os.path.join(BASEDIR,dbpath)
#     store = pd.HDFStore(dbpath,mode='a')
#     dfc = store[key]
#     dfc.loc[i_s,col] = newrowvalues
#     store.put(key=key, value=dfc, format='table')
#     store.close()

# def update_row_of_store(dbpath,key, i, newrow, data_columns):
#     # dbpath = os.path.join(BASEDIR,dbpath)
#     store = pd.HDFStore(dbpath,mode='a')
#     dfc = store[key]
#     dfc.loc[i] = newrow
#     store.put(key=key, value=dfc, format='table',data_columns=data_columns)
#     store.close()

# def append_row_to_store(dbpath, key,rowdic):
    
#     df = dptable_getinit(dbpath, key)
#     newindex = df.index.max()+1
#     df.loc[newindex] = rowdic
    
#     dptable_safeput(dbpath, key, df)

#     return newindex

# append_row_to_files_table = partial(append_row_to_store, key='files')
# append_row_to_paths_table = partial(append_row_to_store, key='paths')
# append_row_to_assumed_table = partial(append_row_to_store, key='assumed')
# append_row_to_manual_table = partial(append_row_to_store, key='manual')



# def update_df_slice(dbpath,key, df):
#     # get the store
#     store = pd.HDFStore(dbpath,mode='a')
#     # get the table
#     dfc = store[key]
#     # put the df values in the table
#     dfc.update(df)
#     # put back in database
#     store.put(key=key, value=dfc, format='table')
#     store.close()

# def safe_update_df_slice(dbpath, key,df):
#     #get table
#     table = read_table(dbpath, key)
#     # update with new values
#     table.update(df)
#     # safeput
#     dptable_safeput(dbpath, key, table)




# def dptable_safeput(dbpath, key, df):
#     # make db backup
#     bk_url = f"{dbpath}_bkup"
#     shutil.copyfile(dbpath, bk_url)
#     try :
#         unsafe_put(dbpath, key, df)
#     except :
#         ## restore db backup
#         shutil.copyfile(bk_url,dbpath)
#         print("Unexpected error, no changes applied to db, saving aborted:", sys.exc_info()[0])
#     # rm db backup
#     os.remove(bk_url)

# def unsafe_put(dbpath, key, df):
#     # print("undafe put")
#     # raise KeyError
#     store = pd.HDFStore(dbpath,mode='a')
#     stored_df = store[key]

#     dfcs = set(df.columns)
#     sdfcs = set(stored_df.columns)
#     assert sdfcs == dfcs, "the provided df and the target df in the database differ."

#     for attr in dfcs:
#         if stored_df[attr].dtype != df[attr].dtype:
#             print(f"Store dtype is {stored_df[attr].dtype}, df dtype is: {df[attr].dtype}")
#             try:
#                 df[attr] = df[attr].astype(stored_df[attr].dtype)
#             except:
#                 print(f"ERROR: The dtype of {attr} differes between db and df. I couldn't cast the df into the db's dtype either.")
#                 raise KeyError
#                 return

#     # tableinfo = getattr(const, key)
#     store.put(key=key, value=df, format='table')

#     store.close()




# def table_init(store, key):

#     tableinfo = getattr(const, key)
#     df = pd.DataFrame(columns= tableinfo.TABLECOLS)
#     df.loc[0] = tableinfo.TABLECOLS_fakerow
#     print(df)
#     df = df.astype(dtype= tableinfo.TABLECOLS_dtypesD)
#     df.index.name = "index"
#     store.put(key=key, value=df, format='table',
#         data_columns=tableinfo.TABLECOLS_data_columns)

# def init_dptables(fp,table=None):
#     store = pd.HDFStore(fp,mode='w')

#     if not table:
#         for key in [table.key for table in const.tables]:
#             table_init(store,key)
#     else:
#         table_init(store, table)
#     store.close()


# df1 = storelib.dptable_getinit(config_object.dbpath, 'files')
# df2 = storelib.dptable_getinit(config_object.dbpath, 'rk')


# dfo = pd.merge( left=df1.loc[:,['thash','path']], right=df2, how='left', on='thash')
# print(dfo.head())

# def read_table(dbpath, key):
#     df = pd.read_hdf(dbpath, key)
#     return df

# def dptable_getinit(dbpath, key):
#     # store = pd.HDFStore(dbpath,mode='')
#     try :
#         t = read_table(dbpath, key)
#         return t
#     except KeyError:
#         print("Key error, initializing table")
#         init_dptables(dbpath, key)
#         return dptable_getinit(dbpath, key)    


# table='new'
    
import subprocess
def systemCommand(Command):
    Output = ""
    Error = ""     
    try:
        Output = subprocess.check_output(Command,stderr = subprocess.STDOUT,shell='True')
    except subprocess.CalledProcessError as e:
        #Invalid command raises this exception
        Error =  e.output 

    if Output:
        Stdout = Output.decode()
    else:
        Stdout = []
    if Error:
        Stderr = Error.decode()
    else:
        Stderr = []

    return (Stdout,Stderr)


import sqlite3

class MyDBO(object):
    """A little class that handles the database"""

    def __init__(self, config):
        """On init, we make a connection and a cursor
        When this class falls out of scope, it's __del__
        method is called, elegantly closing the db connection
        """
        dbpath = config.dbpath
        self.BASEDIR = config['DEFAULT']['basedir']

        self._db_connection = sqlite3.connect(dbpath)
        self._db_cur = self._db_connection.cursor()
        self.config_object = config

    # not used
    def commit(self):
        self._db_connection.commit()

    # def query(self, query, params):
    #     return self._db_cur.execute(query, params)

    # put table. Could this be safer?
    def ptable(self, tablename, df, if_exists='fail'):
        df.to_sql(tablename, 
            con=self._db_connection,
            if_exists=if_exists,
            index=True,
             )

    def append_row(self, key,rowdic):
        df = self.dptable_getinit(key)
        newindex = df.index.max()+1
        df.loc[newindex] = rowdic
        self.ptable( tablename=key, df=df, if_exists="replace")
        return newindex

    # the safe method to load a table
    def dptable_getinit(self, key, attempt=0):
        # store = pd.HDFStore(dbpath,mode='')
        try :
            t = self.rtable(key)
            return t
        except KeyError:
            log.warning("Key error, initializing table")
            init_dptables( key)
            return self.dptable_getinit(key)    
        except pd.io.sql.DatabaseError:
            if attempt>0:
                log.warning("pandas error!")
            self.init_dptables( key)
            return self.dptable_getinit(key,attempt=attempt+1)    
            
    def rtable(self,tablename):
        df = pd.read_sql_query(f"SELECT * from {tablename}", self._db_connection,
            index_col="index")
        return df

    def init_dptables(self,table=None):
        if not table:
            for key in [table.key for table in const.tables]:
                self.table_init(key)
        else:
            self.table_init(table)

    def table_init(self, key):
        tableinfo = getattr(const, key)
        df = pd.DataFrame(columns= tableinfo.TABLECOLS)
        df.loc[0] = tableinfo.TABLECOLS_fakerow
        df = df.astype(dtype= tableinfo.TABLECOLS_dtypesD)
        df.index.name = "index"

        self.ptable(key, df, if_exists="fail")


    def upd_rkrow_byhash(self, hash, dict):
        pass

    def process_path(self, path):
        return regex.path_assumptions(path)

    def update_assumptions(self):
        # key = 'assumed'
        # dbpath = config_object.dbpath
        # assert os.path.isfile(dbpath), 'database not found'

        df = self.dptable_getinit("assumed")
        # how to update path assumptions on the table
        # we apply the process_path to our path series
        # this function processes the path and returns a dictionary
        # of 'assumptions' (columns in the main table) and their values
        # In the end we get a series of dictionaries
        thing = df['path'].apply(regex.path_assumptions)

        # we turn that into a dataframe, by applying series
        # to our dictionaries (magic!)
        thing = thing.apply(pd.Series)
        # update the values of df with those of thing
        # if NaN on thing it'll be ignored and the old kept
        df.update(thing)   
        
        # save the updated df
        self.ptable("assumed", df, if_exists='replace')

    def update_rk(self, args, config_object):
        """the reasonable knowledge table
        We make one fow for each unique hash value
        We take into account the assumed and manual annotations
        and produce an annotation quality for the rk table
        """

        # store = pd.HDFStore(config_object.dbpath,mode='a')
        # try to get rk table:
        # rktable = config_object.dbo.dptable_getinit('rk')

        log.info("updating reasonable knowledge of database")
        filestable = self.dptable_getinit('files')
        pathstable = self.dptable_getinit('paths')
        pathstable = pathstable[~pathstable.ignore.astype(bool)]
        assumedtable = self.dptable_getinit('assumed')
        manualtable = self.dptable_getinit('manual')

        
        # a merge table
        # disk_id,inode,mtime,ghash  ...  file_id,path  ignore exists_y
        filepathstable = filestable.merge(pathstable, how='left',left_on='index', right_on='file_id')    
        # mattrt = assumedtable.merge(manualtable, how='left', on="path",suffixes=["_assumed","_manual"])

        rk_rowdicts = []
        # m = maintable.merge(filestable, how='left', on="path",suffixes=["_main","_files"])
        # for all hashes in manual table 
        for mti,mtrow in manualtable.iterrows():
            # init a rk row with the default values from constant
            rkr = const.rk.default_rowD.copy() #TODO wherever rowD is called, i should make it a copy
            hash = mtrow['ghash']
            rkr['ghash'] = hash
            log.debug(f"on checksum {hash}")
            for k in [x for x in rkr.keys() if x not in ['ghash']]:
                # do i have a manual value?
                mv = urk_h1(mtrow, k)
                if mv:
                    # log.debug(f"Found manual value for {k} : {mv}")
                    rkr[k] = mv

                # if not
                # find all matching paths
                # try assumptions
                else:
                    assumptions = urk_assumptions(filepathstable, assumedtable, hash, k)
                    if assumptions:
                        log.debug(f"Found assumptions for {k} : {assumptions}")
                        rkr[k] = assumptions

            # quality
            # status
            log.debug(f"updating quality")
            rkr['quality'] = sum([1 for x in rkr.values() if x not in ["NotSet",False,'False',"NonE","NoneFound"]])
            rkr['quality'] += sum([-1 for x in rkr.values() if x in ["NotSet",False,'False',"NonE","NoneFound"]])
            # print(f"#$############## {rkr['quality']} {rkr.values()}")
            if rkr['done']:
                rkr['quality'] = 100
            
            rk_rowdicts.append(rkr)
            del rkr
        
        ndf = pd.DataFrame(rk_rowdicts)
        log.info(f"Done, storing changes")

        # self.dptable_safeput(config_object.dbpath, 'rk', ndf)
        self.ptable('rk', ndf, if_exists='replace')


        # Whenever we update the reasonable knowledge table
        # we dump the database in csv tables
        # and commit the changes in a local git repo
        # this gives us version control of our data
        self.dump_tables()        

    def dump_tables(self):
        """Dumps db's tables into csv's in target file"""
        where = os.path.join(self.BASEDIR,"db/tables")
        log.debug(f"Dumping database as tables in {where}")

        # for each table name:
        for key in [table.key for table in const.tables]:
            # load the table:
            table = self.dptable_getinit(key)
            filename = f"{key}.csv"
            path = os.path.join(where,filename)
            table.to_csv(path)
            log.debug(f"Table {key} saved.")
        self.git_tables()

    def git_tables(self):
        log.debug(f"Comiting new tables to git")
        import subprocess
        original_path = os.getcwd()
        tabledir = os.path.join(self.BASEDIR,"db/tables")

        os.chdir(tabledir)
        out,err = systemCommand("git add --all")
        if err:
            print(f"Error in add all to git: \n{err}")
        # output = subprocess.check_output("git add --all", shell=True)
        out,err = systemCommand("git commit -m 'update'")

        if err: 
            # this is a git output when there's no changes to commit
            # this is perceived as an error, so we catch it manually and ignore it
            if "nothing to commit, working directory clean" in err:
                pass
            else:
                sys.exit(f"Error in commiting to git: \n{err}")
        
        # try: 
        # except subprocess.CalledProcessError:
        #     # printing in error so that it's visible even in the service logs
        #     log.warning("GIT UPDATE ENCOUNTERED ERRORS:")
        #     print(sys.exc_info()[0], file=sys.stderr)
        #     print(output)
        # go back where we were
        os.chdir(original_path)
    
    def hardlink_all(self):
        """Makes sure all our files (paths existing) have a hardlink backup"""
        log.info("Will make sure all existing files are hardlink-backedup")

        paths = self.dptable_getinit("paths")
        paths = paths[paths.exists.astype(bool)]
        files = self.dptable_getinit("files")
        files = files[files.exists.astype(bool)]

        df = files.merge(paths, left_on='index',right_on='file_id')

        for irow,(fp,gh) in df[['path','ghash']].iterrows():
            self.hl_add(fp,gh)


    def hl_add(self, fp, hash):
        """Take a fp and its hash. If the hash doesn't exist in our
        folder of hl backups, add a new hardlink
        """
        basedir=self.BASEDIR

        hl_folder = os.path.join(basedir, "hardlinks")
        if not os.path.isdir(hl_folder):
            os.mkdir(hl_folder)

        hardlink = os.path.join(hl_folder, hash)
        if not os.path.isfile(hardlink):
            log.debug(f"Storing hardlink backup for {fp} as {hash}")
            os.link( fp, hardlink)
            os.chmod(hardlink, 0o744)

    def __del__(self):
        log.debug("DATABASE CLOSING ELEGANTLY")
        self._db_connection.close()




def urk_assumptions(filepathstable, asstable, hash, key):
    # we get all assumed values for one attribute of one hash
    if key not in asstable.columns:
        return None

    # get the paths that point to the hash
    relevant_paths = filepathstable[filepathstable['ghash']==hash]['path']

    # using those paths, filter the assumed table
    # there, we have stored some assumed values for each path
    # since many paths point to the same hash
    # we can mine all of the paths for possible information
    assumptionsdf = asstable[asstable['path'].isin(relevant_paths)]
    # get the unique values, drop NotSet
    uvs = ["*"+x for x in assumptionsdf[key].unique() if x not in ['NotSet']]
    if len(uvs)==0:
        uvs = ['NonE']
    # we MIGHT have multiples, joing them with a coma
    return ",".join(uvs)
    
    

def urk_h1(mtrow, key):
    if key not in mtrow:
        return None
    # if len(manual_row)>1:
    #     sys.exit("this shouuuldn't happen")
    manual_row = mtrow
    if manual_row[key] not in ['NotSet','None',None]:
        return manual_row[key]
    return None