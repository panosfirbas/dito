#!/usr/bin/env python3
"""
GOES TO THE TOP OF THE CLI
"""
# This script holds the code controlling the command line interface.
# we have the main parse (dito), subparsers/modules (dito rml, dito tables, etc)
# and their commands (i.e. dito tables print)             
# each command points to a 'default function'. When dito runs,
# it loads the configuration object 
# parses the arguments
# then finally calls the chosen function 
# (depending on which command was used by the user)
# and passes the configuration object and the arguments object to the function
# (this happens in __main__.py)

# the functions are found in cli_functions (or clif)
# the cli_texts (clit) script holds some additional strings 
# that we use for the cl interface but take up too much space to be kept in here


from . import cli_functions as clif
from .cli_texts import d as clit
import sys
import argparse

from .store import MyDBO







def cmdline_args(config_object):

    # create the top-level parser
    parser = argparse.ArgumentParser(prog='dito',
        description=clit.dito.description,
        epilog="")
    verbosity = parser.add_argument('-v','--verbosity',metavar="", default=2, type=int, help='The verbosity level of dito. Setting this to 1 prints only warnings, 2 prints\
        info and warnings and 3 prints info warnings and debug messages. Default is 2.')
    # for action in parser._actions:
    #     parts = parser._format_action(action)
    #     if action.nargs == argparse.PARSER:
    #         parts = "\n".join(parts.split("\n")[1:])

    # add subparsers to the main one
    # metavar: https://stackoverflow.com/questions/11070268/argparse-python-remove-subparser-list-in-help-menu
    modules = parser.add_subparsers(dest='modules',title="Available modules",description="",help='',metavar="")  

    

    #ls
    #scan 
    # annotate
    # admin
    # admin.fdup
    # admin.backup



    # LS
    # ################################
    # one of the main subparsers, tables takes care of printint tables and mounting

    # create the parser for the "b" command
    tables = modules.add_parser('ls', help="Explore dito's database.",description=clit.tables.description)
    tables.set_defaults(func=clif.print_tables)
    # set the help menu to 'available commands' instead of 'positional arguments'
    # tables._positionals.title = "available commands:"
    # tables_subs = tables.add_subparsers(title="Available functions", description="", help='', metavar="")

    # print
    # tables_command1 = tables_subs.add_parser('print', help='Prints the tables', description=clit.tables.print.description,formatter_class=argparse.RawDescriptionHelpFormatter)
    tfa = tables.add_argument_group('filtering arguments')
    tables_arg1 = tfa.add_argument('-m','--mode',metavar="", default='rkf', type=str, help=argparse.SUPPRESS) #help='Which table to work with, default is the reasonable knowledge table')
    tables_arg2 = tfa.add_argument('-rgx','--regex',metavar="",nargs='+', default=['.*'], type=str, help='One or more, space separated, regular expressions. Files whose path matches these expressions will be kept.')
    tables_arg3 = tfa.add_argument('-rgxv','--regexv',metavar="",nargs='+', default=None, type=str, help='One or more, space separated, regular expressions. Files whose path matches these expressions will be ignored.')
    tables_arg4 = tfa.add_argument('-f','--filter',metavar="",   nargs='+', default=None, type=str, help='One or more, space separated "attribute:value" pairs. Example: -f organism:Mmusculus technique:ATACseq')
    tables_arg5 = tfa.add_argument('-loc',metavar="", type=str, default=None, help='Filter rows and columns by pandas-like .loc. Example: -loc "indexstart:indexend|path,organism,technique" ')

    tva = tables.add_argument_group('display arguments')

    tables_arg8 = tva.add_argument('-mcw',metavar="", type=int, default=80, help='MaxColumnWidth: By default, only the first 80 letter of any column are show, change this to show full paths and hashes')
    tables_arg9 = tva.add_argument('-mr',metavar="", type=int, default=50, help='MaxRows: By default, only 50 row of the table are show, change this to show more')
    tables_arg10 = tva.add_argument('-sa',metavar="", type=str, default=None, help='sort by ascending. Choose a column, or a comma separated list of columns.')
    tables_arg11 = tva.add_argument('-sd',metavar="", type=str, default=None, help='sort by descending. Choose a column, or a comma separated list of columns.')
    tables_arg12 = tva.add_argument('-g',metavar="", type=str, default=None, help='Condense results by the values of a colum, useful to discover possible the available values, for example which species are logged.')
    tables_arg14 = tva.add_argument('--pathcondence','-pcf',metavar="",dest='pcf', type=int, default=2,help=argparse.SUPPRESS) #help='Condense the printed paths by removing the first N path names. Default is 2 which removes /home/ska/')
    
    tma = tables.add_argument_group('mounting arguments')
    tables_arg14 = tma.add_argument('-mp','--mountpoint', default=None, type=str, help="Where to mount. If the target foler exists, a new subfolder will be created inside it. If the target doesn't exist, it will be created")
    tables_arg15 = tma.add_argument('-mby','--mountby', nargs='+', type=str, default='paths', help='One or more, space separated, attributes based on which files will be grouped in folders.')
    tables_arg16 = tma.add_argument('-ifn','--infilename',nargs='+', type=str, default=None, help='One or more, space separated, attributes which will be included in the filenames.')
    tables_arg17 = tma.add_argument('-ltrim','--lefttrimpath',action='store_true', default=False, help='Set to True to left-trim the paths before mounting')


    tables_arg6 = tables.add_argument('--to-csv', type=str, default=None, help='Put the result in a csv file')
    # tables_arg7 = tables.add_argument("--examples", action="store_true",default=False,help="Show some examples of how to use this command")

    # tables_command3 = tables_subs.add_parser('mount', help='prints the tables', description=clit.mount.description)
    # tables_command3_arg0 = tables_command3.add_argument('-mp','--mountpoint', default="./", type=str, help='where to mount')
    # tables_command3_arg1 = tables_command3.add_argument('-t','--table', default='files', type=str, help='which table to work with, default all')
    # tables_command3_arg2 = tables_command3.add_argument('-f','--filter', default=None, type=str, help='filter the table')
    # tables_command3_arg3 = tables_command3.add_argument('-loc', type=str, default=None, help='filter the table before printing')
    # tables_command3_arg4 = tables_command3.add_argument('-by', type=str, required=True, default='paths', help='')
    # tables_command3.set_defaults(func=mount)
    # improve = modules.add_parser('improve', description='This module helps you improve the annotation of our data')
    # improve_command1 = improve.add_parser('status', help='Displays information about the status of the annotation')
    # improve_command2 = improve.add_parser('status', help='Displays information about the status of the annotation')
    


    # SCAN
    #########
    scanner = modules.add_parser('scan', help="Scan your folders to add files to dito's database.",
        description=clit.scanner.description)

    scanner.add_argument('--path', type=str, default="./", help='Dito will only scan inside this path. Default is your current working directory.')
    scanner.set_defaults(func=clif.filescannerf)


    # ANNOTATE
    #############
    annotate = modules.add_parser('annotate', help="Annotate files in dito's database.", description=clit.annotate.description) 
    
    annotate_common_arg04 = annotate.add_argument('--commit', action='store_true', default=False ,help='Commit your annotations to the database.')  
    annotate_common_arg13 = annotate.add_argument('--v', type=int, default=1, dest="verbosity", help='verbosity')

    required = annotate.add_argument_group('filtering arguments')
    annotate_common_arg01 = required.add_argument('--path',default="./",type=str, help='Only consider files that are downstream of a directory (default is your current working directory.)')
    annotate_common_arg02 = required.add_argument('--regex',nargs='+', default=['.*'], type=str, help='One or more, space separated, regular expressions. Files that match these expressions will be kept.')
    annotate_common_arg03 = required.add_argument('--regexv',nargs='+', default=None, type=str, help='One or more, space separated, regular expressions. Files that match these expressions will be ignored.')
    annotate_common_arg02 = required.add_argument('--inpath',nargs='+', default=None, type=str, help='One or more, space separated, words. Paths that contain these words will be kept.')
    annotate_common_arg03 = required.add_argument('--notinpath',nargs='+', default=None, type=str, help='One or more, space separated, words. Paths that contain these words will be ignored.')
    

    anotgroup = annotate.add_argument_group('display arguments')
    tables_arg8 = anotgroup.add_argument('-mcw', type=int, default=None, help='MaxColumnWidth: By default, the entire value any column is show, change this to shrink column width')
    tables_arg9 = anotgroup.add_argument('-mr', type=int, default=50, help='MaxRows: By default, only 50 row of the table are show, change this to show more')
    tables_arg10 = anotgroup.add_argument('-sa', type=str, default=None, help='sort by ascending. Choose a column, or a comma separated list of columns.')
    tables_arg11 = anotgroup.add_argument('-sd', type=str, default=None, help='sort by descending. Choose a column, or a comma separated list of columns.')
    tables_arg14 = anotgroup.add_argument('--pathcondence','-pcf',dest='pcf', type=int, default=2, help='Condense the printed paths by removing the first N path names. Default is 2 which removes /home/ska/')

    d = get_attr_possible_values(config_object) 
    attrargs = annotate.add_argument_group('annotation arguments')
    annotate_common_arg2 = attrargs.add_argument("-org", "--organism", default=None,type=str, help=f'The organism of the file. Here are some already used values: {d.get("organism",[])}')
    annotate_common_arg3 = attrargs.add_argument("-tiss", "--tissue", default=None,type=str, help=f'The tissue of the file. Here are some already used values: {d.get("tissue",[])}')
    annotate_common_arg4 = attrargs.add_argument("-st", "--stage", default=None,type=str, help=f'The stage of the file. Here are some already used values: {d.get("stage",[])}')
    annotate_common_arg5 = attrargs.add_argument("-tr", "--treatment", default=None,type=str, help=f'The treatment of the file. Here are some already used values: {d.get("treatment",[])}')
    annotate_common_arg6 = attrargs.add_argument("-tec", "--technique", default=None,type=str, help=f'The technique of the file. Here are some already used values: {d.get("technique",[])}')
    annotate_common_arg7 = attrargs.add_argument("-targ", "--target", default=None,type=str, help=f'The target of the file. Here are some already used values: {d.get("target",[])}')
    annotate_common_arg8 = attrargs.add_argument("-repl", "--replicate", default=None,type=str, help=f'The replicate of the file. Here are some already used values: {d.get("replicate",[])}')
    annotate_common_arg9 = attrargs.add_argument("-fn", "--filenum", default=None,type=str, help=f'The filenum of the file. Here are some already used values: {d.get("filenum",[])}')
    annotate_common_arg10 = attrargs.add_argument("-skip", "--skip", default=None,type=str, help=f'The skip of the file. Here are some already used values: {d.get("skip",[])}')
    annotate_common_arg10 = attrargs.add_argument("-tag", "--tags", default=None,type=str, help=f'The skip of the file. Here are some already used values: {d.get("skip",[])}')
    annotate_common_arg11 = attrargs.add_argument('--done', action='store_true', default=False ,help='Mark path as done')

    annotate_common_arg12 = annotate.add_argument('-noi','--noinherit', action='store_true', default=False ,help=argparse.SUPPRESS) #inherits by default
    annotate.set_defaults(func=clif.annotatef)



    # ADMIN
    # This one is a group of functions, contains a supbarser
    ##############
    admin = modules.add_parser('admin', help='scanning related functions.',
        description="to-do")

    admin_subs = admin.add_subparsers(title="Available functions", description="", help='', metavar="")



    # ADMIN rmlint COMMANDS
    # ################################
    # one of the main subparsers, rml takes care of the rmlint reports
    rmlrep = admin_subs.add_parser('rmlint', help='rmlint related functions.',
        description=clit.rml.description)
    
    # set the help menu to 'available commands' instead of 'positional arguments'
    rmlrep._positionals.title = "available commands:"
    rmlrep_subs = rmlrep.add_subparsers(title="Available functions", description="", help='', metavar="")

    # LS
    rmlrep_command3 = rmlrep_subs.add_parser('ls', 
        help="List the previous rmlint report folders. You can then navigate to \
        any folder you want in order to investigate the report's results.",
        description="List available reports")
    rmlrep_command3.set_defaults(func=clif.ls_report)

    # GENERATE NEW
    rmlrep_command1 = rmlrep_subs.add_parser('generate_new', help="Generate a new report, must be run as root.",description=clit.rml.generate.description)
    rmlrep_command1.add_argument('--root', type=str, required=False, default='/home/ska/', help='Where dito will start scanning. Default is /home/ska/')
    rmlrep_command1.set_defaults(func=clif.generate_report)

    # DIGEST
    rmlrep_command2 = rmlrep_subs.add_parser('digest', help="Digest the results of a report.",description=clit.rml.digest.description)
    rmlrep_command2.add_argument('--n', type=int, required=True, help='The number of the report')
    rmlrep_command2.add_argument('--v', type=int, default=0, dest="verbosity", help='verbosity')
    rmlrep_command2.set_defaults(func=clif.digest_report)    


    # more things
    update = admin_subs.add_parser('update', help="Update dito's database", description="Update various fields of the database")
    update_subs = update.add_subparsers(title="Available functions", description="", help='', metavar="")

    update_command1 = update_subs.add_parser('assumptions', help='Update the assumptions table')
    update_command1.set_defaults(func=clif.update_assumptions)
    
    # update rk
    update_command2 = update_subs.add_parser('rk', help='Updates the reasonable knowledge table')
    update_command2.set_defaults(func=clif.update_rk)

    # migrate
    # update_command3 = update_subs.add_parser('migrate', help='TO-DO')
    # update_command3.set_defaults(func=clif.migrate)

    cleandb = admin_subs.add_parser('cleandb', help='Shows you how to remove the db file.')
    cleandb.set_defaults(func=clif.cleandbf)

    hardlinkall = admin_subs.add_parser('hardlinkall', help='Make sure all files as hardlink backuped')
    hardlinkall.set_defaults(func=clif.hardlinkall)

    cleanhardlinks = admin_subs.add_parser('rmhardlinks', help='Safely removes all hardlink backups')
    cleanhardlinks.set_defaults(func=clif.safermhardlinks)

    dobackup = admin_subs.add_parser('dobackup', help='Run the scripts to backup to NAS')
    dobackup.set_defaults(func=clif.backup_to_nas)




    return parser









def main(config_object):

    if sys.version_info<(3,5,0):
        sys.stderr.write("You need python 3.5 or later to run this script\n")
        sys.exit(1)
        
    p = cmdline_args(config_object)

    if len(sys.argv)<2:
        p.print_help(sys.stderr)
    
    # parse the arguments
    # we define a default function in the parsed args    
    pargs = p.parse_args()

    return pargs




#  Functions to produce values from the tables and be used in the help menu texts
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def get_attr_possible_values(config_object):
    """Manually add annotations to paths"""

    # This is loaded when parsing the args, we don't have a db connection yet
    # so this function will make its own little connection
    # before we make the persistent one for the config_object

    dbo = MyDBO(config_object)

    dfm = dbo.dptable_getinit('manual')
    dfa = dbo.dptable_getinit('assumed')

    coi = ['technique', 'treatment', 'target', 'tissue', 'stage', 'organism']
    from .regex import possible_values

    d = {}
    for c in coi:
        s = set([*dfm[c].unique(),*dfa[c].unique(),*possible_values.get(c,[])])
        ss = sorted(s)
        ss.remove('NotSet')
        d[c] = ss

    del dbo

    return d


def get_available_school_topics(config_object):
    from glob import glob
    installdir=config_object['DEFAULT'].get('installdir',None)
    schoolpathdir = os.path.join(installdir,"ditoschool")
    paths = glob(f"{schoolpathdir}/*.md")
    topics = [os.path.split(p)[1][:-3] for p in paths ]
    return topics
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

