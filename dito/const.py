# Some constants to be easily loaded by everything
# used as a setup script for the 'database'

import numpy as np


def myprint(v, s,argsv):
    if v<=argsv:
        print(s)
    else:
        pass




class O(object):

    def __init__(self,key):
        self.key = key
        self.TABLECOLS = []
        self.TABLECOLS_fakerow = []
        self.TABLECOLS_data_columns = []
        self.TABLECOLS_dtypesD = {}
        self.MIN_ITEMSIZE = None
        self.default_rowD = {}

    def add_col(self, name, default_value, dtype, index=False, min_itemsize=None):
        self.TABLECOLS.append(name)
        self.TABLECOLS_fakerow.append(default_value)
        if index:
            self.TABLECOLS_data_columns.append(name)
        
        
        self.TABLECOLS_dtypesD[name] = dtype

        if min_itemsize:
            if not self.MIN_ITEMSIZE:
                self.MIN_ITEMSIZE = {}
            self.MIN_ITEMSIZE[name] = min_itemsize

        self.default_rowD[name] = default_value

tables = []
assumed = O("assumed")
assumed.add_col("path", "nopath", np.unicode_, True,min_itemsize=255)
assumed.add_col("owner","NotSet", np.unicode_)
assumed.add_col("organism","NotSet", np.unicode_)
assumed.add_col("tissue","NotSet", np.unicode_)
assumed.add_col("stage","NotSet", np.unicode_)
assumed.add_col("treatment","NotSet", np.unicode_)
assumed.add_col("technique","NotSet", np.unicode_)
assumed.add_col("target","NotSet", np.unicode_)
assumed.add_col("replicate","NotSet", np.unicode_)
assumed.add_col("filenum","NotSet", np.unicode_)
assumed.add_col("extension","NotSet", np.unicode_)
tables.append(assumed)


rk = O("rk")
rk.add_col("ghash", "None", np.unicode_, True,min_itemsize=128)
# rk.add_col("skip", 0, np.bool, True)
# rk.add_col("exists", 1, np.bool, True)
rk.add_col("done", False, np.bool, True)
rk.add_col("quality", False, np.int64, True)
rk.add_col("status","NotSet" , np.unicode_)
rk.add_col("organism","NotSet" , np.unicode_)
rk.add_col("tissue","NotSet" , np.unicode_)
rk.add_col("stage","NotSet" , np.unicode_)
rk.add_col("treatment","NotSet" , np.unicode_)
rk.add_col("technique","NotSet" , np.unicode_)
rk.add_col("target","NotSet" , np.unicode_)
rk.add_col("replicate","NotSet" , np.unicode_)
rk.add_col("tags","NotSet", np.unicode_)
rk.add_col("filenum","NotSet" , np.unicode_)
rk.add_col("extension","NotSet" , np.unicode_)
tables.append(rk)

manual = O("manual")
# manual.add_col("path", "nopath", np.unicode_, True,min_itemsize=255)
manual.add_col('ghash',"None", np.unicode_,True,min_itemsize=128)
manual.add_col("organism","NotSet", np.unicode_)
manual.add_col("tissue","NotSet", np.unicode_)
manual.add_col("stage","NotSet", np.unicode_)
manual.add_col("treatment","NotSet", np.unicode_)
manual.add_col("technique","NotSet", np.unicode_)
manual.add_col("target","NotSet", np.unicode_)
manual.add_col("replicate","NotSet", np.unicode_)
manual.add_col("tags","NotSet", np.unicode_)
manual.add_col("filenum","NotSet", np.unicode_)
# manual.add_col("skip", 0, np.bool, True)
manual.add_col("done", False, np.bool, True)
tables.append(manual)


files = O("files")
files.add_col('disk_id',0, np.int64)
files.add_col('inode',0, np.int64)
files.add_col('mtime',0, np.float64)
files.add_col('ghash',"None", np.unicode_,True,min_itemsize=128)
files.add_col('exists', 0, np.bool)
files.add_col('size',0, np.int64)
# files.add_col('thash',"nothash", np.unicode_,True,min_itemsize=128)
# files.add_col('path',"/", np.unicode_,True,min_itemsize=255)
# files.add_col('n_links',0, np.int8)
tables.append(files)

paths = O("paths")
paths.add_col('file_id',0, np.int64) # Foreign key, refers to a row of the files table
paths.add_col('path',"None", np.unicode_,True,min_itemsize=255)
paths.add_col("ignore", 0, np.bool, True)
paths.add_col('exists', 0, np.bool)
tables.append(paths)




















# file_TABLECOLS = ['thash','ghash','path','size','inode','disk_id','mtime','n_links','skip','exists']
# file_TABLECOLS_fakerow = ["aaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaa","aaaaaaaaaaaaaa",0,0,0,0,0,1,1]
# file_TABLECOLS_data_columns = ['inode','disk_id','size','skip','exists']
# file_TABLECOLS_dtypesD = {
#         'thash': np.unicode_,
#         'ghash': np.unicode_,
#         'path': np.unicode_,
#         'size': np.int64,
#         'inode': np.int64,
#         'disk_id': np.int64,
#         'mtime': np.float64,
#         'n_links': np.int8,
#         'skip': np.bool,
#         'exists' : np.bool
#         }

# main_TABLECOLS = ("skip","path",
#     "assumed_owner","assumed_organism","assumed_tissue","assumed_stage","assumed_treatment",
#         "assumed_technique","assumed_target","assumed_replicate","assumed_filenum","assumed_extension",
#     "manual_organism","manual_tissue","manual_stage","manual_treatment",
#         "manual_technique","manual_target","manual_replicate","manual_filenum","manual_tags","manual_is_ours")
# main_TABLECOLS_fakerow = ["." for i in range(len(main_TABLECOLS))]
# main_TABLECOLS_fakerow[0] = 1
# main_TABLECOLS_fakerow[-1] = 1

# main_TABLECOLS_data_columns = ['skip','path','manual_is_ours']

# main_TABLECOLS_dtypesD = {
#     "skip" : np.bool,
#     "path" : np.unicode_,
#     "assumed_owner" : np.unicode_,
#     "assumed_organism" : np.unicode_,
#     "assumed_tissue" : np.unicode_,
#     "assumed_stage" : np.unicode_,
#     "assumed_treatment" : np.unicode_,
#     "assumed_technique" : np.unicode_,
#     "assumed_target" : np.unicode_,
#     "assumed_replicate" : np.unicode_,
#     "assumed_filenum" : np.unicode_,
#     "assumed_extension" : np.unicode_,
#     "manual_organism" : np.unicode_,
#     "manual_tissue" : np.unicode_,
#     "manual_stage" : np.unicode_,
#     "manual_treatment" : np.unicode_,
#     "manual_technique" : np.unicode_,
#     "manual_target" : np.unicode_,
#     "manual_replicate" : np.unicode_,
#     "manual_filenum" : np.unicode_,
#     "manual_tags" : np.unicode_,
#     "manual_is_ours" : np.bool,   
# }
# rk_TABLECOLS = ['thash','skip','exists',"organism","tissue","stage","treatment",
#         "technique","target","replicate","filenum","extension"]

# rk_TABLECOLS_fakerow = ["fakethash!!",0,1,"NotSet","NotSet","NotSet","NotSet","NotSet","NotSet","NotSet","NotSet","NotSet"]
# rk_TABLECOLS_data_columns = ['thash','exists']
# rk_TABLECOLS_dtypesD = {
#         'thash': np.unicode_,
#         'skip': np.bool,
#         'exists' : np.bool,
#         "organism" : np.unicode_,
#         "tissue" : np.unicode_,
#         "stage" : np.unicode_,
#         "treatment" : np.unicode_,
#         "technique" : np.unicode_,
#         "target" : np.unicode_,
#         "replicate" : np.unicode_,
#         "filenum" : np.unicode_,
#         "extension" : np.unicode_,
#         }

























