
import logging
from logging.handlers import RotatingFileHandler
import os




def main(args,config):    
	"""
	In this, we configure some logging with some standard python library helpers

	We make a stream logger, to handle logs to be shown in the sys.std,

	"""
	# create formatter
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')



	# this is the logger
	stream_log = logging.getLogger('dito')
	stream_log.setLevel(logging.DEBUG)

	# Each logger can have multiple handlers
	# The stream handler outputs lines in the terminal.
	# We will later change its LEVEL with an argument from the cli options
	# but for now we initialize it at DEBUG, it will show everything
	ch = logging.StreamHandler()
	
	
	if args.verbosity>2:
		ch.setLevel(logging.DEBUG)
	elif args.verbosity >1:
		ch.setLevel(logging.INFO)
	else:
		ch.setLevel(logging.WARNING)
	# add formatter to ch
	ch.setFormatter(formatter)
	# add ch to logger
	stream_log.addHandler(ch)

	# Here we setup a different logger, to save lines in a file log
	# it's automatically rotated 
	logpath = os.path.abspath(os.path.join( config['DEFAULT']['BASEDIR'],"logs/dito.log"))
	db_handler = RotatingFileHandler(logpath, maxBytes=5000000,backupCount=25)
	db_handler.setFormatter(formatter)
	db_handler.setLevel(logging.INFO)
	stream_log.addHandler(db_handler)

	# Now whenever stream_log is called, it sends its logs to both handlers
	# the file handler will only print INFO + level of logs
	# the stream handler will work based on the verbosity argument
	
	
	


if __name__ == '__main__':
    main()