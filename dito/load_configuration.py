# Allows some configuration options to be passed through a file
# things like: "set the database path at: "

import configparser
import os
import numpy as np
from . import const

def init_config_file(targetfilepath, d, level='DEFAULT'):
    try:    
        config = configparser.ConfigParser()    
        with open(targetfilepath,'w') as fo:
            for k,v in d.items():
                in1=input(v[0])
                config[level][k] = in1 or v[1]
            config.write(fo)
        return config
    except:
        raise(f"Could not initialize config file {targetfilepath}")








class Object(object):
    pass


def get_config_object(BASEDIR):
    import pandas as pd
    from . import store as storelib
    """We need some basic configuration options.    
    The most important one is the 'basedir' where all relevant files will be stored.
    If the default file /path/to/where/dito/is/installed/setup_options.ini
    does not exist, we create one.

    In that file, the basedir is defined. This leads us to the 
    /path/of/basedir/extra_options.ini which the user can easily access and configure

    """
    config = configparser.ConfigParser()
    
    CONFIGFILE = os.path.join(BASEDIR,'dito_configuration.cfg')
    #if configfile exists, try to read it
    if not os.path.isfile(CONFIGFILE):
        # # CONFIGFILE doesn't exist: create it
        # d = {
        #     'basedir' : ("Please set a base folder for dito, where relevant data will be kept (important)","/home/ska/panos/ditobasedirtest/")
        # }
        # init_config_file(CONFIGFILE, d, level='DEFAULT')

        raise(f"Error. I expected a configuration file in {CONFIGFILE}")
    
    # file exists
    config.read(CONFIGFILE)
    config['DEFAULT']['installdir'] = os.path.dirname(os.path.realpath(__file__))
    config.CONFIGFILEPATH = CONFIGFILE
    # print(f"loaded config file:{CONFIGFILE}")
    
    ###########BASEDIR
    BASEDIR=config['DEFAULT'].get('basedir',None)
    # configfile exists but doesn't define basedir
    # which is weird cause i should have made it myself
    assert BASEDIR, f"Could not find a basedir definition in {CONFIGFILE}"

    # is the basedir folder doesn't exist, create it
    if not os.path.isdir(BASEDIR):
        os.mkdir(BASEDIR)
    
    ###########DBNAME
    config['DEFAULT']['dbname'] = os.path.abspath(os.path.join( BASEDIR,"db/database.sql"))
    config.dbpath = config['DEFAULT']['dbname']

    return config

def main():
    # default where i am installed
    INSTALDIR = os.path.dirname(os.path.realpath(__file__))

    # I expect a configuration file two directories above my installation path
    # /home/ska/dito/dito_src/dito is where my code exists
    # /home/ska/dito is where i expect the config file
    tar = os.path.abspath( os.path.join(INSTALDIR, "../..") )

    # A config file needs to be there
    # it points to my isntalation folder where more options are set
    config = get_config_object(tar) # load configuration options from ini files.
    return config


if __name__ == '__main__':
    main()