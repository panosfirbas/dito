import sys
import argparse
import os
import pandas as pd
import numpy as np

import glob
from functools import partial

from . import const
from . import store as storelib


import uuid
import json
import subprocess



import logging 
log = logging.getLogger('dito')



def init_nr(fp):
    import os
    try:
        fi = os.open( fp, os.O_RDWR )
        info = os.fstat(fi)
        infod = {"size": info.st_size,
                "inode": info.st_ino,
                "disk_id":info.st_dev,
                "mtime":info.st_mtime,
                'n_links':info.st_nlink
                }
        os.close(fi)

        rni = { 'path' : fp,
                'size' : infod['size'],
                'inode' : infod['inode'],
                'disk_id' : infod['disk_id'],
                'mtime' : infod['mtime'],
                'n_links' : infod['n_links'],
                'exists' : 1
                }
    except FileNotFoundError:
        # the file doesn't currently exist in the system
        rni = { 'path' : fp,
                'size' : 0,
                'inode' : 0,
                'disk_id' : 0,
                'mtime' : 0,
                'n_links' : 0,
                'exists' : 0
                }
    except PermissionError:
        # the file doesn't currently exist in the system
        rni = { 'path' : fp,
                'size' : 0,
                'inode' : 0,
                'disk_id' : 0,
                'mtime' : 0,
                'n_links' : 0,
                'exists' : "NoPermission"
                }
    return rni


def are_same_record(row, nr_init):
    if row['inode']==nr_init['inode'] \
        and row['disk_id'] == nr_init['disk_id']\
        and row['size'] == nr_init['size']\
        and row['mtime'] == nr_init['mtime']:
        return True
    else:
        return False

def lazy_ghash(fp, nr_init, d=False):
    # avoid calculating if possible
    # d comes from the rmlint report
    #  if it fits our observations, we trust the hash
    if d \
        and d.get("checksum",None) \
        and (d['size']==nr_init['size'])  \
        and d['inode']==nr_init['inode'] \
        and d['disk_id'] == nr_init['disk_id']:
        # the rmlint report can be trusted, we take the hash from it:
        log.debug("trusting in rmlint's hash, everythign checks out")
        return d['checksum']
    else :
        # else we calculate it
        return file_blake2b(fp)
def file_blake2b(fp):
    import hashlib
    """Produces the hash of a target file"""
    # return uuid.uuid4().hex #hack to make things fast
    log.debug(f"hashing {fp}")
    
    m = hashlib.blake2b()
    with open(fp, 'rb') as fi:
        for chunk in iter(lambda: fi.read(4096), b""):
                m.update(chunk)
    return(m.hexdigest())










class Filescanner(object):

    def __init__(self, config_object,verbosity=5):
        self.config_object = config_object
        self.dbo = config_object.dbo


    def scanpath(self, tpath):
        code_root = os.path.join(self.config_object['DEFAULT']['installdir'], "findfqgz")
        log.debug(f"Filescanner, setting up for {tpath}")
        npath = os.path.abspath(tpath)
        os.chdir(code_root)
        cmd = f"bash ./findfqgz.sh {npath}"
        # import sys
        # print(cmd)
        # sys.exit("...")
        popen = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, universal_newlines=True)
        for stdout_line in iter(popen.stdout.readline, ""):
            yield stdout_line.rstrip() 
        popen.stdout.close()
        return_code = popen.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd)

    #  dbpath, args, d=None


    def digest_fp(self,fp):
        """Handles incoming filepaths 
        checks if there's already a record of the file and if not, 
        creates the record

        files table:
        # each row, is a node containing specific data
        # since only one inode exists on the system at any one time
        # and it can only point to one file
        # only one row per inode can be marked as exists at any time
        # exists means at least one path is pointing to this inode
        # and disk/mtime/size are found on the filesystem just as they are on the row
        index disk_id inode mtime ghash exists size

        paths table:
        # here, file_id is a key of the files table
        # this way, each row of this table, is a path pointing to a row of the files table
        # exists means the path exists and points to file_id row
        index file_id path ignore exists
        """

        # define a print that takes verbosity levels into consideration
        # if the vprint 1st argument is <= vervisity it will be printed
        # this way we can set verbosity with --v

        
        
        # clean a newline char
        fp = fp.rstrip()
        log.info(f"Digesting path: {fp}")

        # get some basic current meta data from the file
        # if file doesn't exist we get an object result anyway
        
        log.debug(f"Touching the file")
        nr_init = init_nr(fp) 

        # local copies of the db tables
        filesdf = self.dbo.dptable_getinit('files')
        pathsdf = self.dbo.dptable_getinit('paths')

        # if the file doesn't exist right now (our init_nr returned so)
        # nothing is found on the filesystem, so we don't have an inode
        if not nr_init['exists']:
            log.info("File doesn't exist")

            # see if we have a record of the path, then set it to not exists
            # any connection to this path doesn't exist anymore
            lslice = pathsdf.loc[pathsdf['path']==fp].copy()
            if len(lslice)>0:
                log.info("Saving path as non existing in db")
                lslice['exists'] = False       
                pathsdf.update(lslice) # save the change in our local df
                self.dbo.ptable( 'paths', pathsdf, if_exists='replace')
            del lslice
            # and we're done here, nothing more to do for the path
            return 0
        if nr_init['exists'] == 'NoPermission':
            print(f"\nWarning: You don't have access rights for file \n{fp}\nBecause of this, I can't access its inode, and it will be ignored. Try running as root")
            return 0
            
        # what do i know of this inode?
        # find rows with the same inode

        # is there a putative direct hit?
        putative = filesdf[(filesdf.inode == nr_init['inode']) & filesdf['exists']]
        # only one row can 'exist' at any time
        if len(putative)>1:
            sys.exit("this shouldnt hapen")
        if len(putative)>0:
            log.debug("Putative hits for inode found")
            putativerow = putative.iloc[0]
            # i have a putative hit. a row with the same inode that i think exists
            if are_same_record(putativerow,nr_init):
                # yep they're the same. the file_id i need is the index of the row
                log.debug("Perfect match")
                if not putativerow['exists']:
                    # mark the inode row as existing
                    filesdf.loc[putativerow.name,'exists'] = True
                    self.dbo.ptable( 'files', filesdf, if_exists='replace')
                    log.info("Saving inode as non existing in db")
                file_id = putativerow.name
            else:
                # the row doesn't exist as we knew it anymore, 
                # since our new file is on the inode, and it's not the same as the previous
                # file, then the previous file doesn't exist.
                # update on local df and on database
                
                log.info("Node has changed, updating database")
                filesdf.loc[putativerow.name,'exists'] = False 
                self.dbo.ptable( 'files', filesdf, if_exists='replace')

                # have to make new row
                file_id, _rowd = self.new_files_row(fp, nr_init)
                filesdf.loc[file_id] = _rowd
        else:
            # never seen this inode before
            # make a new row
            log.info("First time I see this inode, updating database")
            file_id, _rowd = self.new_files_row(fp, nr_init)
            filesdf.loc[file_id] = _rowd


        # i now have the file_id index. this means i know which data it is i'm handling
        # and now i can deal with the other part of the 'equation', the path
        
        # again, a putative hit has the same path and we think it exists
        putative = pathsdf[(pathsdf.path == fp) & pathsdf['exists']]
        if len(putative)>1:
            # there can only be one 'exists' per path
            sys.exit("this should not happen")
        if len(putative)>0:
            log.debug("Putative hits for path found")
            putativerow = putative.iloc[0]
            # so we have a putative direct hit
            # that's a line in the db, saying that pathX was pointing to inodeX
            # if the record we have for the path was
            # pointing to the same inode (that's the file_id)
            # then it's a clean hit, we already know this path and inode
            if putativerow.file_id == file_id:
                log.debug("Perfect match")#the file we're handling is exactly the same as the one in our records
                path_id = putativerow.name
                if not putativerow['exists']:
                    # weirdly the now existing path was already in our db but marked as non existing
                    # maybe a file that was moved and then moved back ?
                    log.info("Path was found in database but was marked non existing. updated to existing.")
                    pathsdf.loc[putativerow.name,'exists'] = True 
                    self.dbo.ptable( 'paths', pathsdf, if_exists='replace')
            else:
                # the row doesn't exist anymore, 
                # the path exists but it points to a different inode
                # update that it doesn't exist on local df and on database
                # that is, the connection of the path to the inode doesn't exist
                log.info("Path to inode connection has changed, updating database")
                pathsdf.loc[putativerow.name,'exists'] = False 
                self.dbo.ptable( 'paths', pathsdf, if_exists='replace')
                # now have to make a paths row
                path_id, _rowd = self.new_paths_row(fp,file_id)
                pathsdf.loc[path_id] = _rowd
        else:
            # now have to make a paths row
            log.info("This is a new path, updating database")
            path_id, _rowd = self.new_paths_row(fp,file_id)
            pathsdf.loc[path_id] = _rowd


        return 0

    def new_files_row(self,fp, nr_init):
        """we add a new row to the files table"""
        # take a default row from the constants script
        _rowd = const.files.default_rowD.copy()
        # update its values with values from nr_init
        _rowd.update((k, nr_init[k]) for k in _rowd.keys() & nr_init.keys())
        log.debug("Getting the file's checksum")
        _rowd['ghash'] = lazy_ghash(fp, nr_init)
        file_id = self.dbo.append_row(key='files',rowdic=_rowd)
        log.info("appended new line to the files table")

        # try to add a hl backup. if the hash existed already, no harm done
        self.dbo.hl_add(fp, _rowd['ghash'])

        # and add a manual table row
        manualt = self.dbo.dptable_getinit('manual')
        # the manual table is based on the checksum (ghash), not the inode
        if not _rowd['ghash'] in manualt['ghash'].values:
            # get a default row
            mainrow = const.manual.default_rowD.copy()
            # update the checksum
            mainrow['ghash'] = _rowd['ghash']
            # add to db
            indexnum = self.dbo.append_row(key='manual',rowdic=mainrow)    
            log.info("appended new line to the manual annotations table")

        # and finally a rk table row
        rkt = self.dbo.dptable_getinit('rk')
        # the rk table is based on the checksum (ghash), not the inode
        if not _rowd['ghash'] in rkt['ghash'].values:
            # get a default row
            mainrow = const.rk.default_rowD.copy()
            # update the checksum
            mainrow['ghash'] = _rowd['ghash']
            # add to db
            indexnum = self.dbo.append_row(key='rk',rowdic=mainrow)    
            log.info("appended new line to the rk annotations table")

        # file id is the new row's index number, it'll be used to add a path table row
        # rowd is the new row in dictionary form
        return file_id,_rowd


    def new_paths_row(self,fp,file_id):
        _rowd = const.paths.default_rowD.copy()
        upd = {'file_id' : file_id,
        'path' : fp,
        'ignore' : False,
        'exists' : True}
        _rowd.update(upd)
        # path_id = storelib.append_row_to_paths_table(dbpath=dbpath,rowdic=_rowd)
        path_id = self.dbo.append_row(key='paths',rowdic=_rowd)
        log.info("appended new line to the paths table")

        # if the path is not found in the 'assumptions' table, add a row:
        assumed_table = self.dbo.dptable_getinit('assumed')
        
        if not _rowd['path'] in assumed_table['path'].values:
            # get a default row
            mainrow = const.assumed.default_rowD.copy()
            assumptions = self.dbo.process_path(_rowd['path'])
            mainrow['path'] = _rowd['path']
            mainrow.update((k, assumptions[k]) for k in mainrow.keys() & assumptions.keys())
            # add to db
            indexnum = self.dbo.append_row(key='assumed',rowdic=mainrow)    
            log.info("appended new line to the assumed annotations table")

        return file_id,_rowd
