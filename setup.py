import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "dito",
    version = "0.8",
    author = "Panos Firbas",
    author_email = "panosfirbas@protonmail.com",
    description = ("Handle your data"),
    license = "GPL3",
    keywords = "",
    # url = "https://github.com/PanosFirmpas/bamToBigWig",
    packages=['dito'],
    package_data={'dito': ['dito/dito/findfqgz/*']},
    include_package_data=True,
    # install_requires=['numpy', 'pysam', 'pyBigWig', 'SharedArray'],    
    # long_description=read('README.md'),
    entry_points={
          'console_scripts': [
              'dito = dito.__main__:main'
          ]
      },
    classifiers=[
        "Development Status :: Beta",
        "Topic :: Utilities"
    ],
)
