Dito helps with the discovery of duplicates, the cataloging available files, the annotation of files, the discovery of available files through their annotation, the backup of files via hardlinks, and finally facilitates the backing up to our NAS.

Simple users will use this to add their new files to the database ('dito scan'), annotate them ('dito annotate'), or to discover files from the database ('dito ls').        
Those discovered files can then be mounted anywhere on the system, for the user to have easy read access to them.
Administrators can use the 'dito admin' functions which handle duplicate discovery (via rmlint 'dito admin rmlint'), backups ('dito admin backup'), and general database actions ('dito admin db').              
                
Please feel free to use the ['issues'](https://gitlab.com/panosfirbas/dito/-/issues) page of the repository to ask questions or help.


<!-- automagically generated on gitlab -->
# Table of Contents
[[_TOC_]]















# Theory


## A few words on files and linux filesystems

Imagine you have a file, let's call it 'file1.fq.gz'.
What your linux system does is, it stores the actual bits of the file on a specific position of the disk and then it keeps a note that 'file1.fq.gz' points to that position. These positions are known as inodes. We can imagine that our file exists on inode1.          
At this point, we have our bits on inode1, and a known name for those bits: file1.fq.gz            
          
|inode (contains the bits) | name  |
|---|---|
| 1  | file1.fq.gz  |

In addition the filesystem keeps track of the filesize and the time when the file was last changed:

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  |

When you move, or rename a file, your system simply changes the name in its database:

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | newfilename.fq.gz  |

But if you copy a file, your system will allocate a second inode, copy the bits to it and create a record of the new name:

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  |
| 2  | 32b | 02.01.2020 | file1copy.fq.gz  |

You can now independently edit the file1copy, changing the bits of inode2, without affecting file1.fq.gz.

An inode can be associated with more than one names:

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  |
| 1  | 32b | 01.01.2020 | file2.fq.gz  |

In this case, file2 points to the same inode as file1. It is what is known as a hardlink. We created a hardlink to the contents of inode1 and named it file2. The original name, file1 is also a hardlink. Usually we only call something a hardlink when it is added as a secondary link to an already existing file.

**Attention**. If you edit the contents of file2 in the above example, the contents of file1 will also be edited.
















## A few words on hashes

Imagine a magic box with the following property: you can put whatever you want inside it, and the box will reply with a number that is absolutely unique to the item that you put in the box. The slightest change in your object will make the box give you a whole new number. 
This is what hashing algorithms do for digital files. We run our file (the item) through the algorithm (the box) and the algorithm gives us a number (checksum) that is unique to the file. We use this to confirm the validity of files; for example, when I download some large fastq file from a public database, I can calculate the checksum of the file and compare it to the one that was provided by the database. If the checksums match, it means I have a bit-by-bit perfect copy of the original file.

In the case of copied files, like the example above, the checksum of both files will be the same since they are copies:

|inode (contains the bits) | size | lastChanged| name  |checksum|
|---|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  | a211bc |
| 2  | 32b | 02.01.2020 | file1copy.fq.gz  | a211bc |

## Duplicate Discovery with hashes

Say Juan had a file from a zebrafish experiment, and I copied it to my home to work with it:

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | juan/file.fq.gz  |
| 2  | 32b | 02.01.2020 | panos/file1.fq.gz  |

Notice that because I copied the file, we are now spending 32b more diskspace in the system, to store my copy in inode2.
Since I don't plan to edit the file, a better idea would be to make my file a hardlink to juan's. This way inode2 can be released and the space will be free for other files.

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | juan/file.fq.gz  |
| 1  | 32b | 01.01.2020 | panos/file1.fq.gz  |

This is simple enough when we have two known files, but our system has multiple users and years worth of redundant copies.         
How can we find copied files?

|inode (contains the bits) | size | lastChanged| name  |
|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  |
| 2  | 32b | 01.01.2020 | file2.fq.gz  |
| 3  | 12b | 02.01.2020 | file3.fq.gz  |
| 4  | 32b | 01.01.2020 | file4.fq.gz  |

In the example above, we have 4 files. File3 is obviously different, so we don't need to worry about it being a copy. Files 1,2 and 4 seem identical, but with just a filesize and an access date, we cannot be 100% sure that two files are identical.

The trick is to use hashing. We compute the checksum for each interesting file:

|inode (contains the bits) | size | lastChanged| name  | checksum |
|---|---|---|---|---|
| 1  | 32b | 01.01.2020 | file1.fq.gz  | a211bc |
| 2  | 32b | 01.01.2020 | file2.fq.gz  | 1bcxx8 |
| 3  | 12b | 02.01.2020 | file3.fq.gz  ||
| 4  | 32b | 01.01.2020 | file4.fq.gz  | a211bc |

It turns out that file2 is indeed different from file1. But file4 is a copy and could potentially be replaced by a hardlink to save space.














## Dito's tables

Dito keeps the necessary information in a number of tables.

### files table
This table creates a row for each unique file in the filesystem: ['disk_id','inode','mtime','ghash','exists','size'].        
Hardlinks point to the same inode, so they only take up one row in this table.        
If dito knows of a file on an inode, but detects a different file (different size/mtime/disk_id) on that inode,
the old record is marked as non existant (exists=0).

### paths table
This table stores the connection between paths and unique files: ['file_id','path','ignore','exists']          
Each row, means that on the filesystem, there exists a "path" that points to the "file_id" inode.          
If a path's target inode changes, the old row is marked as non existant.


### assumed table
["path","owner","organism","tissue","stage","treatment","technique","target","replicate","filenum","extension"]

This table stores a number of properties, assumed by dito. Dito reads any path in its database
and with some regular expressions we have setup will assume some of the annotation of the file, such as the organism.
To keep things tidy, these unsafe annotatios are kept separate from the manual annotation table

### manual table
['ghash',"organism","tissue","stage","treatment","technique","target","replicate","tags","filenum","done"]

This table stores the manual annotations that users have provided for files. Notice that these manual
annotations are stored on the checksum "ghash" of the files. This means that when a user annotates a path,
dito finds the checksum of that path and annotates that. If any other user had a that same file in their
home folder, with a different path, then that file will be automagically annotated as well, without the second
user having to manually annotate their own file.


### reasonable knowledge table
["ghash","done","quality","status","organism","tissue","stage","treatment","technique","target","replicate","tags","filenum","extension"]

This table is a combination of the assumed and manual tables. If a file has manual annotation, then it's used, otherwise, dito searches for 
any paths that point to this checksum and tries to use their assumed annotations.






























# Getting started





## As an admin

### RMLINT
In our lab, fastq files are very rarely edited, but they are often copied. This situation is a great opportunity to employ hardlinks in order to save space.
We employ rmlint, a great tool which is designed for duplicate discovery to detect duplicates and replace them with hardlinks.

If you're not familiar with the term hardlink or if you feel like going a bit deeper, please read the [Theory](#Theory) section of this document.
There, terms like hardlink, inode, checksums are discussed in more detail.

We don't NEED to run rmlint, but it's best to do it before our first big scan of the system with dito. The reason for that,
is that after we use rmlint to replace duplicates, those files are readily recognized as duplicates and dito will not waste
time scaning them. 

#### How it works
Rmlint works in steps. First it scans the system, or a list of files. We only want to deal with fastq files so we run a bash script with a complex "find" (the linux command line tool "find")
command which produces a list of all .fastq.gz or fq.gz files in the /home/ path.        
We feed this list of fastqs to rmlint who tests putative copies with their checksum. Confirmed copies are reported in an rmlint report and appended on an rmlint script. 

After the rmlint scan is finished, we can review the script and run it. This will replace the confirmed copied files with hardlinks, saving us disk space.            

#### Doing it
To run an rmlint report on the whole system:


```sh
# This is a lengthy process, so I recommend doing it in a linux screen
>> screen -S rmlscan
# Make sure you are logged in as root, this is needed because 
# the script will be "reading"(hashing the bits to make the checksum) everyone's files.
>> su
# we can now call the dito command
>> dito admin rmlint generate_new --root /home/ska
# you should now see the rmlint working. It will take a while.
# you can not pres "Control+a" then 'd' to exit the screen
```

After some time passes, we go back to our screen to see if the job is done:

```sh
>> screen -r rmlscan
# If the job has finished, we can ask dito to list all the reports it has run:
>> dito admin rmlint ls
# and we'll see something like this:

/home/ska/ditobasedirtest/rml_reports/report_8_2020-10-05_13:50:50.333936
/home/ska/ditobasedirtest/rml_reports/report_4_2020-06-24_13:37:02.105951
/home/ska/ditobasedirtest/rml_reports/report_5_2020-06-24_13:39:50.360686
/home/ska/ditobasedirtest/rml_reports/report_10_2020-10-05_13:51:58.072143
/home/ska/ditobasedirtest/rml_reports/report_1_2020-06-05_15:39:45.811981
/home/ska/ditobasedirtest/rml_reports/report_9_2020-10-05_13:51:11.460709
/home/ska/ditobasedirtest/rml_reports/report_2_2020-06-24_13:13:24.541351
/home/ska/ditobasedirtest/rml_reports/report_7_2020-07-30_15:58:11.010874
/home/ska/ditobasedirtest/rml_reports/report_3_2020-06-24_13:13:37.753037
/home/ska/ditobasedirtest/rml_reports/report_6_2020-06-24_13:42:38.485430

# report number10 is the latest one, we can now navigate in that folder:
>> cd /home/ska/ditobasedirtest/rml_reports/report_10_2020-10-05_13:51:58.072143

>> ls -lh

ocsv.csv 			# The report's data in csv format
ojson.json 			# The report's data in json format. 
					# Only duplicated files are reported here. Hardlinks 
					# are reported, softlinks no. 
osh.sh 				# The report's bash script, run this to make the hardlinks suggested by rmlint.
ostdout.stdout      # The stdout output of the report
ouniques.uniques    # This is a list of filepaths, no duplicates where found for these.
```

You should now open 'osh.sh'. This is the big product of the rmlint scan, it's a shell script
that when run will implement the changes suggested by rmlint.             
Dito's rmlint command, configures rmlint to look for duplicated files and replace them with hardlinks.
More details on rmlint can be found here: https://github.com/sahib/rmlint            
           
Once you are satisfied that rmlint worked properly and that the script will make good changes to our system,
you can execute the script:

```sh
>> bash osh.sh
```
### Scan

As an admin, you can issue a system-wide scan from dito. This uses the same fastq.gz script
as the one we used with rmlint, but this time the paths are evaluated by dito and added to dito's 
database.         
This will be a slow process, especially the first time it is run, since the checksums of all files
need to be generated. Thankfully, the process can be stopped and resumed safely, since once a file
is added to the database successfully, it doesn't need to be scanned again.

```sh
>> cd /home/ska
>> dito scan /home/ska
```





## As a user

### Scan
If you are not an admin (you don't have sudo), dito will not be able to scan the whole system
but you can use the scan function to add any new files you might have just gotten into the 
database

```sh
>> cd /home/user/
>> dito scan /home/user/my_new_data_folder
```









### Annotate

With annotate, we setup manual annotations for our files. We use the command's filtering options
to filter a list of files, then we apply annotations on all these files.      


For ease of use, this command considers files in dito's database that are downstream of your 
current working directory. For example, running 'dito annotate' in your home folder will
only consider files found in your home folder.
Let's imagine you just added two folders with new data, and used 'dito scan' to add 
them to dito's database

:exclamation: :exclamation: Make sure to read the 'dito annotate --help' menu. In there,
you can find previously used annotations. Using these will keep annotations consistent
and help with the system's health (i.e. if we call ATAC experiments "ATACseq" don't annotate yours as "ATAC-seq").
:exclamation: :exclamation:


```sh
>> cwd
/home/user/data

>> ls -lh
/new_folder_one/
/new_folder_two/

>> dito annotate

	path done organism tissue stage treatment technique target replicate filenum    tags
0	...data/new_folder_one/zebrafish_ATACseq_dome_r1.fastq.gz    0     NonE   NonE  NonE      NonE      *ATACseq   NonE      NonE    NonE  NotSet
1	...data/new_folder_one/zebrafish_ATACseq_dome_r2.fastq.gz    0     NonE   NonE  NonE      NonE  *ATACseq   NonE      NonE    NonE  NotSet
2	...data/new_folder_two/mouse_liver_r1.fastq.gz    0     NonE   NonE  NonE      NonE  NonE   NonE      NonE    NonE  NotSet
3	...data/new_folder_two/mouse_liver_r2.fastq.gz    0     NonE   NonE  NonE      NonE  NonE   NonE      NonE    NonE  NotSet
```
We only have 4 files into consideration, all of them ATACseq but in different organisms. Dito figured out that the zebrafish experiments are ATACseq from the path,
but we can go ahead and mark all these files as ATACseq manually:

```sh
>> dito annotate --technique ATACseq 
# you will be shown the changes and prompted to commit them,
# if you are already sure, you can already pass the commit argument:
>> # dito annotate --technique ATACseq --commit
```


Next, we'll mark the replicates:

```sh
# --inpath _r1 only keeps the files whose path contains '_r1'
>> dito annotate --inpath "_r1" --replicate rep1 --commit
>> dito annotate --inpath "_r2" --replicate rep2 --commit
```

Then the organisms, stages and tissues
```sh
>> dito annotate --inpath "mouse" --organism Mmusculus --stage adult --tissue liver --commit
>> dito annotate --inpath "zebra" --organism Drerio --stage dome --tissue whole_embryo --commit
```

Now our annotations should be visible in dito's database with 'dito ls'

Click the image for some of dito annotate in action:

[![asciicast](https://asciinema.org/a/01BD2K3kg6LaTLkQtSvORTF7x.png)](https://asciinema.org/a/01BD2K3kg6LaTLkQtSvORTF7x)





### Discover

Dito's main purpose is to facilitate the discovery of existing files, based on their annotation/properties.

The 'dito ls' command filters the available files based on your filtering optinos, and outputs a table
based on your display options. 

For example, let's look for mouse atac-seq experiments:

```sh
>> dito ls -f organism:Mmusculus
# I have both chipseq and atacseq results
# to keep only the atac seq ones
>> dito ls -f organism:Mmusculus technique:ATACseq
# I can further refine my search, to only include RA cells from the tissue column
>> dito ls -f organism:Mmusculus technique:ATACseq,tissue:RAcells
```

You can also filter the paths with regular expressions:

```sh
# for example, the following will only keep files
# containing the string o_mouse in their path
>> dito ls -rgx ".*o_mouse.*"
```

To fit the results in the terminal, some adjustments need to be made. You can change things
like the max rows, or max width of a row with the available options. You can also sort your rows.        
If you want to see the available values in any one column, try the -g option, it will give you 
an overview of what values have been used in that column.       


You can save the results in a csv file with the --to-csv option.

Click this image to see some of 'dito -ls' in action

[![asciicast](https://asciinema.org/a/5zJE9Kv2FdcBUuXaIoIdFO27L.png)](https://asciinema.org/a/5zJE9Kv2FdcBUuXaIoIdFO27L)



### Mounting

The , essentially a list of files, can be mounted on your filesystem
by using the 'mount' option. This will create hardlinks to the already existing files, allowing you easy access 
for your bioinformatic needs.

For example

```sh
# make a new folder in /home/ska/panos
>> cd /home/ska/panos
>> mkdir test
>> cd test
# the following will create a folder : /home/ska/panos/data
# and in there, it will create symbolic links to the files that were in you 'dito ls' results
>> dito ls --organism Mmusculus -mp ./data
```

This might not always be useful since it sometimes creates very deep trees. Try the -ltrim option
to trim down unecessary folders in your mounting.

Alternatively, you can mount files by their attributes. You can select how to split them in folders
with the -mby option, and which attributes to include in the filename with the -ifn option.

For example

```sh
# I want to mount the mouse files in a subfolder called 'mdata'
>> #dito ls -f organism:Mmusculus -mp ./mdata
# I will break it down in folders based on organism and tissue
>> #dito ls -f organism:Mmusculus -mp ./mdata -mby organism tissue
# In the filenames, I will include technique replicate filenum
# the final command:
>> dito ls -f organism:Mmusculus -mp ./mdata -mby organism tissue -ifn technique replicate filenum
```
Here, you can see dito ls's mounting in action:

[![asciicast](https://asciinema.org/a/33ViXuVqbsOaQzVR0LUymlnjR.png)](https://asciinema.org/a/33ViXuVqbsOaQzVR0LUymlnjR)











